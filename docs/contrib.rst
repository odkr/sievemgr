============
Contributing
============

.. highlight:: bash

Contributions are welcome, `SASL mechanisms`_ in particular.
If you want to contribute another feature, consider `filing feature
request <https://github.com/odkr/sievemgr/issues/new>`_ first,
so that we can discuss things beforehand.


Style
=====

Please target Python version 3.9 and follow :pep:`8`, :pep:`20`, :pep:`257`
as well as the spirit of Robert Pike's `Notes on Programing in C
<https://doc.cat-v.org/bell_labs/pikestyle>`_. Substantial contributions
should follow the SOLID_ design pattern.


Tests
=====

Code should pass the test suite before being submitted.
Please add tests for your code, too.

Run the test suite::

    make check

Skip slow tests::

    make quickcheck

Set the `checks` macro to override which tests are run, for example::

    make checks=tests.test_acap check


Linters
=======

SieveManager ships with configurations for:

* Bandit_
* Dlint_
* Flake8_
* MyPy_
* Ruff_
* PyLint_
* Pyright_
* vermin_
* vulture_

It would be kind if you tested your code with Bandit *or* Dlint to check for
security issues, MyPy *or* Pyright to check for type errors, Ruff *or* Pylint
to check the coding style, and vermin to check whether your code runs in
Python 3.9.

Install all of the above, save for Pyright::

    pip3 -r linters.txt

Run all linters::

    make lint

Run MyPy and Bandit only::

    make quicklint

Set the `lint` macro to override which files are linted, for example::

    make lint=sievemgr.py lint


Automation
==========

Run

.. code:: bash

    git config core.hooksPath .githooks

to run :command:`make quickcheck quicklint` before each commit
and :command:`make check lint` before each push.

.. note::

   :file:`.githooks` only ships with the Git respository.
   It is not included in the tarball.


Version Control
===============

SieveManager uses Git_ and follows the `Git Flow`_ branching model.
The source is hosted at `Codeberg <repo_>`_ and mirrored at
`repo.or.cz <repo-mirror>`_ (secondary).


Pull Requests
=============

`Fork SieveManager <https://codeberg.org/odkr/sievemgr/fork>`_ on Codeberg.

Download the ``devel`` branch of your fork::

    git clone --branch devel "git@codeberg.org:${username:?}/sievemgr.git"

Set up SieveManager as upstream source::

    git remote add upstream git@codeberg.org:odkr/sievemgr.git

Create a branch for your changes::

    git checkout -b feat-foo

Make your changes::

   vi sievemgr.py

Commit, rebase, and push::

    git add sievemgr.py
    git commit -S -m 'feat: added support for foo'
    git pull --rebase upstream devel
    git push origin feat-foo

.. important::
   Please sign your commits.

Finally, submit your pull request.


Patches
=======

You can also send a patch to the :file:`@zoho.com`
address listed in my `PGP key`_.

Download the ``devel`` branch::

    git clone --branch devel "git@codeberg.org:odkr/sievemgr.git"

Set up SieveManager as upstream source::

    git remote add upstream git@codeberg.org:odkr/sievemgr.git

Create a branch for your changes::

    git checkout -b feat-foo

Make your changes::

    vi sievemgr.py

Commit and rebase::

    git add sievemgr.py
    git commit -S -m 'feat: added support for foo'
    git pull --rebase upstream devel

Create a patch::

    git diff HEAD HEAD~1 >foo.patch

Please sign your patch::

    gpg -ab foo.patch
