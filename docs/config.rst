=============
Configuration
=============

.. highlight:: none

.. program:: sievemgr


Syntax
======

The configuration consists of key-value pairs and comments, each of which
must comprise a line of its own. Keys and values are separated by whitespace.
Comments start with a hash ("#"). Comments, empty lines, and leading or
trailing whitespace are ignored.


Keys
====

.. confvar:: account [login@]host

    Subsequent statements only apply to accounts matching `login` and `host`.
    If `login` is omitted, all accounts on `host` match.

.. confvar:: alias string

    Use `string` as alias for the current host.

    For example, use

    .. code-block:: none
        :emphasize-lines: 3

        account imap.foo.example
            login jdoe
            alias foo

    to connect to :samp:`imap.foo.example` as :samp:`jdoe` with:

    .. code:: bash

        sievemgr foo

.. confvar:: backups n

    Keep `n` backups of Sieve scripts.

    `n` = 0
        Do not make backups (default).

    `n` = 1
        Back up :file:`file` as :file:`file~`.

    `n` > 1
        Back up :file:`file` as :file:`file.~{m}~`, where *m* starts
        with 1 and increments with each backup. Old backups are
        deleted if there are more than `n` backups.

.. confvar:: cafile file

    Load custom certificate authorities (CAs) from `file`.
    `File` must be in PEM format.

.. confvar:: cadir dir

    Load custom certificate authorities (CAs) from `dir`. `Dir` must
    contain one PEM file per CA, each of which must be named after the
    hash of its subject name.

    .. only:: man

        See :manpage:`c_rehash(1)` for details.

    .. only:: not man

        See the OpenSSL manual (fn. SSL_CTX_load_verify_locations_)
        for details.

.. confvar:: cert file

    Read the client TLS certificate from `file`. `File` must be in PEM
    format and contain the private key, the client certificate, and every
    CA certificate required to establish the certificate's authenticity
    -- in that order. Some ManageSieve servers also require that each CA
    certificate is followed by its certificate revocation list.

.. confvar:: confirm command [, ...]

    Comma-separated list of commands that require confirmation
    if they overwrite or remove a file.

    Either a combination of:

    * :sievecmd:`cp`
    * :sievecmd:`get`
    * :sievecmd:`mv`
    * :sievecmd:`put`
    * :sievecmd:`rm`

    Or one of:

    * ``all`` (default)
    * ``none``

.. confvar:: getpassphrase command

    Read the TLS key passphrase from the standard output of :samp:`{command}`.

.. confvar:: getpassword command

    Read the login password from the standard output of :samp:`{command}`.

.. confvar:: host hostname

    Host to connect to (default: :samp:`localhost`).

.. confvar:: key file

    Read the client TLS key from `file`, rather than from the certificate file.
    `File` must be in PEM format.

.. confvar:: login username

    Log in as *username*.

.. confvar:: ocsp

    Check whether the server's TLS certificate has been revoked using OCSP_?

    ``yes`` (default) or ``no``

    .. warning::
        .. include:: snippets/ocsp.rst

    .. warning::
       The default of this setting may change in the future.

.. confvar:: password string

     Use `string` as password.

     :file:`sieve.cf` must not be group- or world-readable
     if a `password` is set.

     .. danger::
         Passwords should be stored in encrypted form only.
         Use :confvar:`getpassword` instead.

.. confvar:: port port

    Connect to `port` (default: 4190).

.. confvar:: timeout float

    Connection timeout in seconds.

.. confvar:: tls boolean

    Secure connections with Transport Layer Security (TLS)?

    ``yes`` (default) or ``no``.

    .. danger::
        Data sent over an unsecured connection can be
        read and modified by third parties.

.. confvar:: saslmechs mechanism [, ...]

    Comma-separated list of authentication mechanisms, ordered by preference.

    External mechanism:

    * ``external``

    Password-based mechanisms:

    * ``scram-sha3-512-plus``
    * ``scram-sha-512-plus``
    * ``scram-sha-384-plus``
    * ``scram-sha-256-plus``
    * ``scram-sha-224-plus``
    * ``scram-sha-1-plus``
    * ``scram-sha3-512``
    * ``scram-sha-512``
    * ``scram-sha-384``
    * ``scram-sha-256``
    * ``scram-sha-224``
    * ``scram-sha-1``
    * ``plain``
    * ``cram-md5`` (obsolete)
    * ``login`` (obsolete)

    :manpage:`sh(1)`-like patterns are expanded to mechanisms in the above
    order. However, ``EXTERNAL`` authentication, if requested, is always
    attempted before password-based authentication.

    Default: ``scram-*, plain``

.. confvar:: saslprep credentials

    Types of `credentials` that should be normalised.

    One of:

    * ``usernames``
    * ``passwords``
    * ``all`` (default)
    * ``none``

    Adjust if valid credentials are rejected.

.. confvar:: verbosity level

    One of:

    * ``error``
    * ``warning``
    * ``info`` (default)
    * ``debug``
    * ``auth`` (show authentication exchange)

    The higher the level, the fewer messages are printed,
    where ``error`` is highest and ``auth`` lowest.

    .. danger::
        The authentication exchange likely contains your password,
        even though this is not apparent. Do *not* share it.

.. confvar:: x509strict boolean

    Reject TLS certificates that do not conform to :rfc:`5280`?

    ``yes`` (default) or ``no``.


Filenames
=========

:samp:`~` and :samp:`~{user}` are expanded to the home directory of
the logged in and the given `user` respectively, but only if they
occur at the start of a filename.

Relative filenames are interpreted as being relative to the directory of the
configuration file in which a filename variable is set or, if the variable
is set with :option:`-o`, the current working directory.


Commands
========

:samp:`~` and :samp:`~{user}` are expanded in the same way as
they are expanded in filenames.

:samp:`${var}` and :samp:`$\\{{var}\\}` are expanded to the configuration
variable `var`. '\$' can be escaped by prefixing it with another '\$'
(e.g., :samp:`$$var` is expanded to :samp:`$var`).

Commands are split into words before :samp:`~` and variables are
expanded. Otherwise, they are split in the same way as they
are split by :manpage:`sh(1)`.


Passwords
=========

Passwords and passphrases can be read from the standard output of system
commands with :confvar:`getpassword` and :confvar:`getpassphrase`.

GnuPG
-----

GnuPG_ can also be used as a simple password manager.

First, create a directory to hold the encrypted passwords::

    mkdir -m 0700 ~/.passwords

Next, encrypt the password for each account with GnuPG and put the encrypted
file in that directory. For example, to encrypt the password for :samp:`user`
on :samp:`imap.host.example` for the private key :samp:`0123abcd`::

    gpg -er 0123abcd <<EOF >~/.passwords/user@imap.host.example.gpg
    <password>
    EOF

Finally, add

.. code:: none

    getpassword gpg -d ~/.passwords/$login@$host.gpg

to your :file:`sieve.cf` to read passwords from such files.


KeePassXC
---------

First, add the password for :samp:`user` on :samp:`imap.host.example` to the
directory :samp:`mail` in :file:`database.kdbx`::

    keepassxc-cli add database.kdbx mail/user@imap.host.example -p

Then add

.. code:: none

    getpassword keepassxc-cli show -a password database.kdbx mail/$login@$host

to your :file:`sieve.cf` to look up passwords in the folder :file:`mail` of
the KeyPass database file :file:`database.kdbx` using KeePassXC_.


MacOS Keychain
--------------

Add

.. code:: none

    getpassword security find-internet-password -s $host -a $login -w

to your :file:`sieve.cf` to look up passwords stored by Apple Mail or
MailMate_ in the macOS' Keychain.

Passwords can be added to the Keychain with::

    security add-internet-password -s host.imap.example -a user -w

.. warning::
   Granting :command:`security` access to a password, effectively,
   grants *every* application access to that password.


Pass
----

First, add the password for :samp:`user` on :samp:`imap.host.example`::

    pass insert mail/user@imap.host.example

Then add

.. code:: none

    getpassword pass mail/$login@$host

to your :file:`sieve.cf` to look up passwords in the folder
:file:`mail` of a pass_ password store.


Files
=====

:file:`/etc/sieve/config`, :file:`/etc/sieve.cf`, :file:`{$XDG_CONFIG_HOME}/sieve/config`, :file:`{$HOME}/.sieve/config`, :file:`{$HOME}/.sieve.cf`
    Default configuration files.


Security
========

TLS should *not* be disabled.

.. include:: snippets/pwmgrs.rst


.. include:: snippets/privacy.rst


Examples
========

Recommended configuration::

    # Keep a single backup
    backups 1

    # Only require confirmation for removing scripts
    confirm rm

    # Be less verbose
    verbosity warning

Accounts::

    account imap.foo.example
        alias foo
        login user

    account imap.bar.example
        alias bar
        login user@bar.example


Use TLS client authentication to log in as :samp:`user`
on :samp:`imap.foo.example`::

    account imap.foo.example
        login user
        cert client.crt
        key client.key
        saslmechs external


.. only:: man

    See Also
    ========

    :manpage:`sievemgr(1)`

