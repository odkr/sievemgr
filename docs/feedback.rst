========
Feedback
========

If there is something wrong with SieveManager,
please be so kind and let me know.

If you have discovered a security issue, please write an encrypted email
to the :file:`@zoho.com` address listed in my `PGP key`_. Otherwise, I'd
prefer if you submitted your feedback to the `issue tracker`_. But you
are welcome to write me an email, too.

When submitting a bug report, please include:

* What system you use

* Your Python version

* What you were trying to do

* Steps to reproduce the bug, if possible

* Debugging output (enable with :option:`-d <sievemgr -d>`)

