############
SieveManager
############

.. image:: _static/coverage.svg
   :target: _static/coverage/index.html
   :alt: Coverage data

.. image:: https://www.bestpractices.dev/projects/8336/badge
   :target: https://www.bestpractices.dev/en/projects/8336
   :alt: OpenSSF best practices badge

Sieve_ is a programming language for filtering email. Sieve scripts are
typically run by the mail server when mail is delivered to an inbox,
so they need to be managed remotely.

SieveManager is a command-line client and Python module for uploading,
downloading, and managing remote Sieve scripts using the ManageSieve
protocol.

It aims to be convenient, secure, and portable,
and respects Unix as well as Python conventions.

.. warning::
    The command-line interface, the configuration semantics,
    and the Python API may still change.


Example
=======

Upload and activate a Sieve script:

.. code:: none

    $ sievemgr user@imap.foo.example
    user@imap.foo.example's password: <password>
    sieve://user@imap.foo.example> put script.sieve
    sieve://user@imap.foo.example> activate script.sieve

In Python:

.. code:: python

    from sievemgr import SieveManager
    with SieveManager('imap.foo.example') as mgr:
        mgr.authenticate('user', 'password')
        with open('sieve.script', mode='br') as script:
            mgr.putscript(script, 'sieve.script')
        mgr.setactive('sieve.script')


Features
========

* Fully implements :rfc:`5804` (ManageSieve protocol)

* Login can be automated using:

  * Password managers
  * GnuPG-encrypted password files
  * :doc:`Configuration files <config>`
  * `.netrc <netrc_>`_

* Password-based authentication with:

  * CRAM-MD5
  * LOGIN
  * PLAIN
  * SCRAM-\* and SCRAM-\*-PLUS [#untested]_ with

    * SHA-1
    * SHA-2-234
    * SHA-2-256
    * SHA-2-384
    * SHA-2-512
    * SHA-3-512

* TLS client authentication

* Proxy authentication

* Powerful tab-completion

* Highly scriptable

* Emacs-like backup of scripts

* Checks whether TLS certificates have been revoked
  (using lightweight OCSP_)

* Supports giving IPv6 addresses on the command-line


.. [#untested] SCRAM-\*-PLUS authentication is untested.


.. toctree::
    :hidden:
    :titlesonly:

    quick
    install
    command
    config
    module
    security
    feedback
    contrib
    Change log <changelog>
    License <license>
    genindex
