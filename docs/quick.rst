**********
Quickstart
**********

.. highlight:: bash

Choose the operating system that you want to install SieveManager on:

.. highlight:: bash

..
    .. tab:: Alpine Linux

        Add the repository key::

            doas curl -o /etc/apk/keys/odkr@codeberg.org.rsa.pub \
                https://codeberg.org/api/packages/odkr/alpine/key

        Add the SieveManager repository::

            doas tee -a /etc/apk/repositories <<EOF
            https://codeberg.org/api/packages/odkr/alpine/3.13/sievemgr
            EOF

        Install SieveManager::

            doas apk add sievemgr

.. tab:: Debian

    Add my Debian repository::

        sudo curl -o /etc/apt/trusted.gpg.d/org_codeberg_odkr.asc \
            https://codeberg.org/api/packages/odkr/debian/repository.key
        sudo tee /etc/apt/sources.list.d/org_codeberg_odkr.list <<EOF
        deb https://codeberg.org/api/packages/odkr/debian bullseye main
        EOF
        sudo apt-get update

    Install SieveManager::

        sudo apt-get install sievemgr

    .. tip::
        These instructions also work for Debian-*based* distributions
        (e.g., Linux Mint, MX Linux, Pop!_OS, Ubuntu, or Zorin).


.. tab:: Other Unix-like

    Install SieveManager::

        pip3 install sievemgr

See :doc:`install` for details, troubleshooting, and a signed tarball.
