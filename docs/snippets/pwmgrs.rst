Passwords can be queried from password managers to automate logins. However,
any command that can be run by :command:`sievemgr` can, at the very least,
also be run by any application that can run :command:`python`.
