Privacy
=======

.. include:: snippets/ocsp.rst

OCSP is enabled by default. Set :confvar:`ocsp` to `no` to disable it.
