.. _APT: https://debian-handbook.info/browse/en-US/stable/apt.html
.. _Bandit: https://bandit.readthedocs.io
.. _Dlint: https://duo.com/blog/introducing-dlint-robust-static-analysis-for-python
.. _Flake8: https://flake8.pycqa.org
.. _Git: https://git-scm.com
.. _IANA: https://www.iana.org
.. _KeePassXC: https://keepassxc.org
.. _MailMate: https://freron.com
.. _MyPy: https://www.mypy-lang.org
.. _OCSP: https://en.wikipedia.org/wiki/Online_Certificate_Status_Protocol
.. _PyLint: https://www.pylint.org
.. _Pyright: https://microsoft.github.io/pyright
.. _Python: https://www.python.org/
.. _Ruff: https://docs.astral.sh/ruff
.. _SOLID: https://web.archive.org/web/20131109054242/http://www.objectmentor.com/resources/articles/Principles_and_Patterns.pdf
.. _SSL_CTX_load_verify_locations: https://www.openssl.org/docs/manmaster/man3/SSL_CTX_load_verify_locations.html#DESCRIPTION
.. _Sieve: http://sieve.info
.. _YAML: https://www.yaml.org
.. _`Git Flow`: https://nvie.com/posts/a-successful-git-branching-model
.. _`GnuPG`: https://gnupg.org
.. _`PGP key`: https://keys.openpgp.org/vks/v1/by-fingerprint/8975B184615BC48CFA4549056B06A2E03BE31BE9
.. _`Package Installer for Python`: https://pypi.org/project/pip/
.. _`Python Package Index`: https://www.pypi.org/
.. _PyCA: https://github.com/pyca/
.. _`SASL mechanisms`: https://www.iana.org/assignments/sasl-mechanisms/sasl-mechanisms.xhtml
.. _`SRV records`: https://en.wikipedia.org/wiki/SRV_record
.. _`Server Name Indication`: https://en.wikipedia.org/wiki/Server_Name_Indication
.. _`Simple Authentication and Security Layer`: https://en.wikipedia.org/wiki/Simple_Authentication_and_Security_Layer
.. _`Zsh completion`: https://zsh.sourceforge.io/Doc/Release/Completion-System.html
.. _`context manager`: https://docs.python.org/3/reference/datamodel.html#context-managers
.. _`issue tracker`: https://github.com/odkr/sievemgr/issues
.. _bash-completion: https://github.com/scop/bash-completion
.. _cryptography: https://cryptography.io
.. _dnspython: https://www.dnspython.org
.. _emacs-backup: https://www.gnu.org/software/emacs/manual/html_node/emacs/Backup.html
.. _gnu-makefile-conv: https://www.gnu.org/prep/standards/html_node/Makefile-Conventions.html#Makefile-Conventions
.. _netrc: https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html
.. _pass: https://www.passwordstore.org
.. _repo-mirror: https://repo.or.cz/sievemgr.git
.. _repo: https://codeberg.org/odkr/sievemgr
.. _vermin: https://github.com/netromdk/vermin
.. _vulture: https://github.com/jendrikseipp/vulture
