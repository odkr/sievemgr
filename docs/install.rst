************
Installation
************

.. highlight:: bash


License
=======

SieveManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SieveManager is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the :doc:`GNU General Public
License <license>` for more details.


Requirements
============

SieveManager runs on every Unix-like system (e.g., Linux or macOS) that
runs Python_ ≥ v3.9. It has been tested on Alpine Linux, Debian, Darwin,
FreeBSD, NetBSD, and OpenBSD.

Installation from the Python Package Index (PyPI)
requires the `Package Installer for Python`_ (PIP).

..
    Alpine Linux
    ============

    The package targets Alpine Linux ≥ 3.13. It may work on older versions, too.

    Add the repository key::

        doas curl -o /etc/apk/keys/odkr@codeberg.org.rsa.pub \
            https://codeberg.org/api/packages/odkr/alpine/key

    Add the repository::

        doas tee -a /etc/apk/repositories <<EOF
        https://codeberg.org/api/packages/odkr/alpine/3.13/sievemgr
        EOF

    Install SieveManager::

        doas apk add sievemgr


Debian
======

The package targets Debian ≥ v11 ("bullseye") and distributions based on
Debian ≥ v11 (e.g., versions of Linux Mint, MX Linux, Pop!_OS, Ubuntu, or
Zorin released since ca. 2022). It may work on older versions, too.

Add the repository key::

    sudo curl -o /etc/apt/trusted.gpg.d/org_codeberg_odkr.asc \
        https://codeberg.org/api/packages/odkr/debian/repository.key

Add the repository::

    sudo tee /etc/apt/sources.list.d/org_codeberg_odkr.list <<EOF
    deb https://codeberg.org/api/packages/odkr/debian bullseye main
    EOF

Update the package index::

    sudo apt-get update

Install SieveManager::

    sudo apt-get install sievemgr


Python Package Index
====================

Install SieveManager::

    pip3 install sievemgr

.. note::
    The PyPI package ships *without* the manual pages for :doc:`sievemgr
    <command>` and :doc:`sieve.cf <config>` and the `shell completion
    scripts <EXTRAS_>`_.

Troubleshooting
---------------

The most common reason for :command:`pip3 install sievemgr` to fail is that
PyCA_ does not offer a pre-compiled wheel of the cryptography_ module for
your system.

Install SieveManager with

.. code:: bash

    pip3 install --no-deps sievemgr

instead.

.. danger::
    SieveManager works witout :mod:`cryptography`. But it will *not*
    be able to check whether TLS certificates has been revoked.


Tarball
=======

Preparation
-----------

Import my `PGP key`_, so that you can check whether
the tarball has been tampered with::

    gpg --keyserver keys.openpgp.org --recv-keys 6B06A2E03BE31BE9


Download
--------

Download the tarball:

.. code-block::
    :substitutions:

    version="|release|"
    baseurl="https://codeberg.org/odkr/sievemgr/releases/download/v$version"
    tarball="sievemgr-$version.tgz"
    curl --no-clobber -O "$baseurl/$tarball" -O "$baseurl/$tarball.asc"

Check whether the tarball has been tampered with:

.. code-block::
    :substitutions:

    gpg --keyserver keys.openpgp.org --recv-keys 6B06A2E03BE31BE9
    gpg --verify sievemgr-|release|.asc

Unpack the tarball:

.. code-block::
    :substitutions:

    tar xzf sievemgr-|release|.tgz

Go to the package directory to proceed with the installation:

.. code-block::
    :substitutions:

    cd sievemgr-|release|

.. TIP::
    Subscribe to https://codeberg.org/odkr/sievemgr/releases.rss
    to be notified about new releases.


.. highlight:: bash

.. include:: ../INSTALL.rst


Extras
======

Bash completion
---------------

The :download:`Bash completion script <../completions/sievemgr.bash>`
should work automatically if bash-completion_ is installed.

However, it can also be loaded by adding, for example,

.. code:: bash

    source "$HOME/.local/share/bash-completion/completions/sievemgr.bash"

to your :file:`.bashrc`.


Zsh completion
--------------

The :download:`Zsh completion script <../completions/sievemgr.zsh>`
targets Zsh's `modern completion system <Zsh completion_>`_.

It can be activated by adding, for example,

.. code:: zsh

    fpath+=("$HOME/.local/share/zsh/site-functions")

    autoload -Uz compinit
    compinit

to your :file:`.zshrc`.

