*************
Python module
*************

Synopsis
========

.. code:: python

    from sievemgr import SieveManager


Description
===========

.. autoclass:: sievemgr.SieveManager
    :exclude-members: collect, execute, isconn, receiveline, sendline
    :inherited-members:

.. autoclass:: sievemgr.Capabilities

.. autoclass:: sievemgr.LocalScripts

.. autoclass:: sievemgr.Response
    :special-members: __str__

.. autoclass:: sievemgr.URL
    :special-members: __str__

.. autoclass:: sievemgr.Atom

.. autoclass:: sievemgr.Line

.. autoclass:: sievemgr.Word


SASL
====

.. note::
    You need not read this section unless you want to implement an
    `authentication mechanism <SASL mechanisms_>`_.

.. mermaid::
    :caption: Authentication architecture

    sequenceDiagram
        participant auth as authenticate()
        participant mech as :SASL mechanism
        participant conn as :SASL adapter
        auth ->> mech: .__init__(conn, authcid, authzid, ...)
        activate auth
        activate mech
        mech ->> mech: Prepare credentials
        break Encoding invalid
            mech --) auth: ValueError
        end
        deactivate auth
        deactivate mech
        auth ->> mech: .__call__()
        activate auth
        activate mech
        mech ->> conn: .begin(name, ...)
        activate conn
        deactivate conn
        loop SASL exchange
            mech ->> conn: .receive()
            activate conn
            conn --) mech: Message
            deactivate conn
            mech ->> conn: .send(message)
            activate conn
            deactivate conn
        end
        mech ->> conn: .end()
        activate conn
        break Authentication failed
            conn --) auth: OperationError
        end
        conn --) mech: Capabilities
        deactivate conn
        mech --) auth: Capabilities
        deactivate mech
        auth ->> mech: .authcid
        mech --) auth: Authentication ID
        auth ->> mech: .authzid
        mech --) auth: Authorisation ID
        deactivate auth

.. autoclass:: sievemgr.BaseAuth
    :special-members: __call__, __init__

.. autoclass:: sievemgr.BaseSASLAdapter

.. autoclass:: sievemgr.BasePwdAuth
    :special-members: __init__

.. autoclass:: sievemgr.BaseScramAuth

.. autoclass:: sievemgr.BaseScramPlusAuth

.. autoclass:: sievemgr.AuthzUnsupportedMixin

.. autoclass:: sievemgr.CramMD5Auth
    :members: name, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ExternalAuth
    :members: name, order
    :special-members: __init__
    :exclude-members: exchange

.. autoclass:: sievemgr.LoginAuth
    :members: name, order
    :exclude-members: exchange

.. autoclass:: sievemgr.PlainAuth
    :members: name, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA1Auth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA1PlusAuth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA224Auth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA224PlusAuth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA256Auth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA256PlusAuth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA384Auth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA384PlusAuth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA512Auth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA512PlusAuth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA3_512Auth
    :members: name, digest, order
    :exclude-members: exchange

.. autoclass:: sievemgr.ScramSHA3_512PlusAuth
    :members: name, digest, order
    :exclude-members: exchange

.. autoenum:: sievemgr.SASLPrep


ERRORS
======

.. autoexception:: sievemgr.Error

.. autoexception:: sievemgr.CapabilityError

.. autoexception:: sievemgr.ConfigError

.. autoexception:: sievemgr.DataError

.. autoexception:: sievemgr.OperationError

.. autoexception:: sievemgr.ProtocolError

.. autoexception:: sievemgr.SecurityError

.. autoexception:: sievemgr.SoftwareError

.. autoexception:: sievemgr.UsageError

.. autoexception:: sievemgr.ClientError

.. autoexception:: sievemgr.ClientConfigError

.. autoexception:: sievemgr.ClientConnectionError

.. autoexception:: sievemgr.ClientOperationError

.. autoexception:: sievemgr.ClientSecurityError

.. autoexception:: sievemgr.ClientSoftwareError

.. autoexception:: sievemgr.OCSPError

.. autoexception:: sievemgr.OCSPDataError

.. autoexception:: sievemgr.OCSPOperationError

.. autoexception:: sievemgr.SASLError

.. autoexception:: sievemgr.SASLCapabilityError

.. autoexception:: sievemgr.SASLProtocolError

.. autoexception:: sievemgr.SASLSecurityError

.. autoexception:: sievemgr.SieveError

.. autoexception:: sievemgr.SieveCapabilityError

.. autoexception:: sievemgr.SieveConnectionError

.. autoexception:: sievemgr.SieveOperationError

.. autoexception:: sievemgr.SieveProtocolError

.. autoexception:: sievemgr.TLSError

.. autoexception:: sievemgr.TLSCapabilityError

.. autoexception:: sievemgr.TLSSecurityError


Example
=======

Patch the active Sieve script of every user:

.. code:: Python

    from contextlib import suppress
    from sievemgr import SieveManager, ExternalAuth
    from subprocess import CalledProcessError, run

    with SieveManager('imap.host.example') as mgr:
        for user in users:
            try:
                mgr.authenticate('admin', owner=user, sasl=(ExternalAuth,))
                with mgr.localscripts((mgr.getactive(),)) as local:
                    activescript, = local.scripts
                    run(['patch', activescript, patchfile], check=True)
            except Exception as err:
                print(err, file=sys.stderr)
                continue
            finally:
                with suppress(SieveOperationError):
                    mgr.unauthenticate()


Security
========

Connections are secured with Transport Layer Security (TLS) by default.
TLS should *not* be disabled.

.. include:: snippets/creds.rst


Privacy
=======

.. include:: snippets/ocsp.rst
