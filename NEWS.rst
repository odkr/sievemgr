*******
Changes
*******

.. program:: sievemgr


0.8
===

* Tab-completion now plays nice with backups.
* Added :option:`-B <ls -B>` to :sievecmd:`ls`.
* Added :option:`-c <vi -c>` and :option:`-R <vi -R>` to :sievecmd:`vi`.
* :option:`-a` now always either adds the active script to the list of input
  scripts or, with :sievecmd:`put`, sets the active script as upload target.
* Added :option:`-s <python -s>` to :sievecmd:`python`.
* :sievecmd:`ls -l <ls>` now terminates its output with :literal:`...`.
* :sievecmd:`ls -1 <ls>` now terminates its output with a blank line.
* Uploading scripts that terminate lines with a carriage return *and* a
  newline, as opposed to either a carriage return *or* a newline, works again.
* Backups are now automatically deleted if an upload failed.
* When overwriting files with :sievecmd:`mv`,
  the configured number of backups is made.
* :option:`-d` enables stack traces again.
* Server warnings about semantic errors in scripts are now displayed.
* Using :sievecmd:`exit` in scripts no longer raises :exc:`StopIteration`.
* Non-matching patterns are reported as error again.
* Output of :option:`-h` is now properly formatted in Python ≥ v3.13.
* Word-splitting now mimics :manpage:`sh(1)`.
* Suppressed warning about naive time comparisons in Python ≤ v3.9.
* Removed the :sievecmd:`about` command.
* Re-factored the Python module.
* Added discussion of privacy issues with OCSP_.


0.7.4.7
=======

* Certificate dates are compared non-naively
  if Python ≥ v3.11 and cryptography_ ≥ v43.0 are available.


0.7.4.6
=======

* The error message for a server failing SCRAM verification
  now displays the host name properly.


0.7.4.5
=======

* If a password manager exits with a non-zero exit status < 127
  (i.e., has been found and did not exit because of a signal),
  the password is prompted for.
* Tab-completion now works for patterns
  (e.g., :samp:`f*` now expands to :samp:`foo`, not :samp:`f*foo`).
* Patterns can now be escaped.


0.7.4.4
=======

* Giving passwords on the command line using "user:password@host"
  works again.
* Files given in configuration variables are found again.
* `SRV records`_ are looked up again.


0.7.4.3
=======

* SieveManager now parses server messages more strictly.
* `SRV records`_ that are longer than 253 characters will now be rejected.
  This mitigates CVE-2024-3651_, which affects :mod:`idna` < v3.7.
  SieveManager depends on :mod:`dnspython`, which depends on :mod:`idna`.
  That said, CVE-2024-3651 is 'only' a denial-of-service vulnerability
  and highly unlikely to affect SieveManager.

.. _CVE-2024-3651: https://github.com/advisories/GHSA-jjg7-2v4v-x38h


0.7.4.2
=======

* Usernames given on the command line override account sections again.
* :option:`-o` overrides account sections again.
* :option:`-c` accepts filenames in the current working directory again.


0.7.4.1
=======

* Tab-completion no longer repeats the last completions
  when an argument cannot be auto-completed.
* Tab-completion for directories works again.


0.7.4
=====

* Configuration variables are no longer expanded in filename variables.
* Removed :sievecmd:`more`'s :option:`-p <more -p>` option.
* A BEL is printed when :kbd:`Tab` is pressed, but there are no completions.
  This may result in an audible, visual, or no notification,
  depending on your setup.
* "EXTERNAL" and password-based authentication mechanisms can now be mixed.
* Tab-completion now also works in :sievecmd:`python`.
* :sievecmd:`put` works again.
* :option:`-c` works again.
* :sievecmd:`ls` now displays filenames that are
  longer than the terminal is wide correctly.
* An error is raised if :confvar:`password` is set,
  but :doc:`sieve.cf <config>` is group- or world-readable.
* An error is raised if :doc:`sieve.cf <config>`
  is group- or world-writable.
* Tab-completion now also works for filenames
  that contain whitespace or patterns.
* Made ManageSieve-parser more robust.
* Made YAML-escaping more robust.


0.7.3
=====

* The ``backup`` configuration variable has been
  replaced by :confvar:`backups`.
* The ``getpass`` configuration variable has been
  renamed to :confvar:`getpassword`.
* The ``getkeypass`` configuration variable has been
  renamed to :confvar:`getpassphrase`.
* The ``confdir`` configuration variable has been removed.
* The number of backups to make can, and must, now be limited.
* Relative filenames in configuration files are now interpreted
  as relative to the directory the configuration file is in.
* Configuration settings can now be read from multiple files.
* :option:`-o` now also accepts :samp:`{key}` and :samp:`no{key}`.
* :option:`-c` can now be given multiple times.
* Duplicate :confvar:`alias`-es now raise an error.
* New configuration variable :confvar:`password`,
  which should not be used.
* Usernames containing "," or "=" are now handled correctly by SCRAMs.
* String preparation of usernames and passwords now handles
  bidirectional strings, display property-changing characters, and
  unassigned code points correctly.
* Multiple :confvar:`account` sections for the same account are now
  merged, instead of later sections overriding earlier ones.
* Bash and ZSh completion scripts work again.
* Errors and ommissions in the documentation.


0.7.2
=====

* :mod:`cryptography` and :mod:`dnspython` are now installed as dependencies.
  SieveManager also runs without them, however.
* ``authmechs`` has been renamed to :confvar:`saslmechs`.
* Passwords are no longer prompted for on the GUI
  if there is no controlling terminal.
* Sieve scripts are now assumed to be encoded in UTF-8
  (as mandated by :RFC:`5228`).
* :sievecmd:`ed` and :sievecmd:`vi` now offer to re-edit scripts with
  syntax errors, instead of throwing them away.
* New command :sievecmd:`about`.
* Prompting for passwords and confirmation works under Linux again.
* Obsolete mechanisms can be selected with :confvar:`saslmechs` again.
* Configuration sections without login names and aliases are now recognised.
* OCSP lookup errors are now handled more gracefully.


0.7.1
=====

* New option :option:`-s`.
* New option :option:`-e`.
* :option:`-C` can no longer be given to shell commands.
* :option:`-i` now only triggers an error if confirmation is required
  and no terminal is available.
* '#' now starts comments in the SieveManager shell.
* SieveManager now comes with a ZSh completion script.
* Configuration files have been renamed.
* Confirmations and passwords are always prompted for on /dev/tty,
  regardless of I/O redirection.
* :sievecmd:`caps`, :sievecmd:`cert`, :sievecmd:`diff`,
  :sievecmd:`ls`, and :sievecmd:`help` no longer pipe output that
  doesn't fit on the screen through a pager.
* Scripted mode is now entered only if standard input is not a terminal.


0.7
===

* New command :sievecmd:`echo`.
* New command :sievecmd:`xargs`.
* New flag :option:`-C`.
* New flag :option:`-i`.
* :sievecmd:`cmp` now also prints output if files are equal.
* :sievecmd:`caps`, :sievecmd:`cert`, :sievecmd:`diff`,
  :sievecmd:`ls`, and :sievecmd:`help` now pipe output that
  doesn't fit on the screen through a pager if standard input
  and output are a terminal.
* More options for :sievecmd:`cmp`, :sievecmd:`cp`, :sievecmd:`diff`,
  :sievecmd:`ls`, :sievecmd:`get`, :sievecmd:`more`, :sievecmd:`mv`,
  :sievecmd:`put`, and :sievecmd:`rm`.
* Tab-completion for local files now also selects directories.
* '\/' is automatically appended to tab-completed directory names.
* :sievecmd:`mget` has been removed.
* :sievecmd:`mput` has been removed.
* :confvar:`stacktrace` has been removed.
* :option:`-f` no longer raises an error.
* Scripts stop immediately if any error occurs.
* YAML output is now prefixed with ``---`` and postfixed with ``...``.
* Scalars in YAML output are now properly quoted if necessary.
* :envvar:`EDITOR`, :envvar:`PAGER`, and :envvar:`VISUAL` are
  now treated as commands, not as filenames.
* Repaired tab-completion for :sievecmd:`get` and :sievecmd:`put`.
* Tab-completion now automatically adds the terminating space.


0.6.1
=====

* Connection timeout can now be set.
* :confvar:`debugauth` has been replaced by the logging level "AUTH".
* The authentication exchange can no longer be logged at the SASL level.
* The revocation status of the server's certificate is now checked,
  provided the non-standard cryptography_ Python module is available.
* Output of :sievecmd:`cert` is now in YAML.
* :sievecmd:`ed` and :sievecmd:`vi` can create files again.

.. _cryptography: https://cryptography.io


0.6
===

* The authentication exchange can now be logged at the SASL level, too.
* TLS can now be disabled. Only do this is if you are a system administrator
  and want to test your server configuration.
* SieveManager now ships with a Bash completion script.
* Added the :sievecmd:`cmp` command.
* Output of :sievecmd:`cert` is now YAML.
* :sievecmd:`ls` now has a `-a <ls -a>` option.
* :sievecmd:`more` now has a `-a <more -a>` option.
* :sievecmd:`edactive` has been replaced with :option:`ed -a`.
* :sievecmd:`viactive` has been replaced with :option:`vi -a`.
* Credentials are no longer looked up for a host's domain if they cannot
  be found for the host.
* :confvar:`requiretls` has been replaced by :confvar:`tls`.
* The ``logins`` value of :confvar:`saslprep`
  has been renamed to ``usernames``.
* :confvar:`showauth` has been replaced by :confvar:`debugauth`.
* TLS certification validity checks are now stricter.
* :mod:`netrc` fails to parse :file:`.netrc` files without a ``password``
  token in Python up to version 3.9. To mitigate this, :file:`.netrc` parse
  errors are now downgraded to warnings when SieveManager is executed by
  Python version 3.9 or earlier.
* Fixed SCRAM-SHA3-512 authentication.


0.5.1
=====

"CRAM-MD5" and "LOGIN" are no longer enabled by default.

This is because both of them are, de facto, obsolete ("LOGIN" is non-standard
and superseded by "PLAIN", "CRAM-MD5" was close to being `declared "historic"
<crammd5-to-historic_>`_ and is superseded by "SCRAM-\*") and because
:RFC:`5804` (sec. 2.1) does not mandate that they must be used over TLS.

They can be enabled by :option:`-o authmechs=scram-*,cram-md5,plain,login`.

.. _crammd5-to-historic: https://datatracker.ietf.org/doc/html/draft-ietf-sasl-crammd5-to-historic-00


0.5
===

First release.
