#!/usr/bin/env python3
"""Common machinery for tests."""

#
# Copyright 2024  Odin Kroeger
#
# This file is part of SieveManager.
#
# SieveManager is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# SieveManager is distributed in the hope that it will be useful,
# but WITHOUT ALL WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SieveManager. If not, see <https://www.gnu.org/licenses/>.
#

#
# Modules
#

from collections.abc import MutableMapping, MutableSequence
from typing import TypeVar, Union, Iterable, Callable

import unittest


#
# Types
#

T = TypeVar('T')
"""Type variable."""

Test = tuple[Iterable, Union[T, type[Exception]]]
"""Test to be run with :func:`runtests`."""


#
# Functions
#

def runtests(unit: unittest.TestCase, func: Callable[..., T],
             tests: Iterable[Test[T]]):
    """Run `tests` for `func` in `unit`."""
    for i, (args, exp) in enumerate(tests):
        with unit.subTest(i=i, args=args, exp=exp):
            try:
                retval = func(*args)
            # pylint: disable=broad-exception-caught
            except Exception as exc:
                if not isinstance(exp, type) or not issubclass(exp, Exception):
                    raise
                unit.assertIsInstance(exc, exp)
                continue
            unit.assertEqual(retval, exp)


def makerunner(func: Callable[..., T], tests: Iterable[Test[T]]):
    """Get :class:`unittest.TestCase` method that runs `tests` for `func`."""
    return lambda unit: runtests(unit, func, tests)


# pylint: disable=unused-argument
def noop(*args, **kwargs) -> None:
    """Do nothing."""
    return None


def walk(data, func: Callable[[T], T]):
    """Apply `func` to each leaf in a data tree."""
    if isinstance(data, MutableMapping):
        for key, value in data.items():
            if (repl := walk(value, func)) is not None:
                data[key] = repl
    elif isinstance(data, MutableSequence) and not isinstance(data, str):
        for i, value in enumerate(data):
            if (repl := walk(value, func)) is not None:
                data[i] = repl
    else:
        return func(data)   # type: ignore
    return None
