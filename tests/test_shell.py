"""Test :class:`sievemgr.BaseShell`."""

#
# Copyright 2024  Odin Kroeger
#
# This file is part of SieveManager.
#
# SieveManager is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# SieveManager is distributed in the hope that it will be useful,
# but WITHOUT ALL WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SieveManager. If not, see <https://www.gnu.org/licenses/>.
#

# pylint: disable=missing-class-docstring,missing-function-docstring


#
# Modules
#

import contextlib
import io
import itertools
import os
import pathlib
import shutil
import sys
import unittest

from io import StringIO
from typing import Final, Iterator, Optional, Sequence
from unittest.mock import patch

sys.path.append(os.path.realpath(pathlib.Path(__file__).parents[1]))

import sievemgr

from sievemgr import (BaseShell, ConfirmEnum,
                      ShellDataError, ShellPattern, ShellWord)
from . import Test, noop, runtests, walk
from .auto.words import AUTOWORDS

# Silence the bell
sievemgr.bell = noop


#
# Functions
#

def addspaces(s: str, n: int = 3) -> Iterator[str]:
    for i in range(n):
        yield s + ' ' * i
        yield ' ' * i + s


def mixcase(s: str) -> Iterator[str]:
    yield from map(''.join, itertools.product(*zip(s.upper(), s.lower())))


#
# Globals
#

COMMANDLINES: Final[tuple[Test[int], ...]] = (
    # Empty
    (('',), 0),
    ((' ',), 0),
    (('\t',), 0),
    (('\n',), 0),
    (('  \n \t',), 0),

    # Simple
    (('foo',), 0),
    (('foo qux',), 1),
    (('foo qux quux',), 2),
    (('foo qux quux quuux',), 3),
    (('bar 0',), 0),
    (('bar 1',), 1),
    (('bar 2',), 2),
    (('bar 3',), 3),

    # Wrong arguments
    (('bar',), sievemgr.ShellUsageError),
    (('bar bar',), ValueError),

    # Non-existing functions
    (('-',), sievemgr.ShellUsageError),
    (('do_',), sievemgr.ShellUsageError),
    (('do_foo',), sievemgr.ShellUsageError),
    (('frobnicate',), sievemgr.ShellUsageError),

    # Aliases
    (('!',), 0),
    (('! foo',), 1),
    (('! foo bar',), 2),
    (('! foo bar baz',), 3),
    (('!foo',), 1),
    (('!foo bar',), 2),
    (('!foo bar baz',), 3),

    # Patterns
    (('foo xyz?',), sievemgr.ShellOperationError),
    (('foo ?*',), 3),
    (('foo f*',), 1),
    (('foo *',), 3),
    (('foo * ',), 3),
    (('foo ?oo',), 1),
    (('foo [f]oo',), 1),
    ((r'foo \*',), 1),
    ((r'foo \?',), 1),
    ((r'foo \[foo]',), 1),
    ((r'foo \**',), sievemgr.ShellOperationError),
    ((r'foo \??',), sievemgr.ShellOperationError),
    ((r'foo \[foo][foo]',), sievemgr.ShellOperationError),
    (('bar xyz?',), sievemgr.ShellUsageError),
    (('bar ?*',), sievemgr.ShellUsageError),
    (('bar f*',), sievemgr.ShellUsageError),
    (('bar *',), sievemgr.ShellUsageError),
    (('bar * ',), sievemgr.ShellUsageError),
    (('bar ?oo',), sievemgr.ShellUsageError),
    (('bar [f]oo',), sievemgr.ShellUsageError),
    ((r'bar \**',), sievemgr.ShellUsageError),
    ((r'bar \??',), sievemgr.ShellUsageError),
)


COMMANDS: Final[tuple[Test[int], ...]] = (
    # Empty
    (('',), 0),     # The test shell has a `do_` method
    ((' ',), sievemgr.ShellUsageError),
    (('\t',), sievemgr.ShellUsageError),
    (('\n',), sievemgr.ShellUsageError),
    (('  \n \t',), sievemgr.ShellUsageError),
    (('', 'foo'), sievemgr.ShellUsageError),
    ((' ', 'foo'), sievemgr.ShellUsageError),
    (('\t', 'foo'), sievemgr.ShellUsageError),
    (('\n', 'foo'), sievemgr.ShellUsageError),
    (('  \n \t', 'foo'), sievemgr.ShellUsageError),

    # Simple
    (('foo',), 0),
    (('foo', 'qux',), 1),
    (('foo', 'qux', 'quux'), 2),
    (('foo', 'qux', 'quux', 'quuux'), 3),
    (('bar', 0), 0),
    (('bar', 1), 1),
    (('bar', 2), 2),
    (('bar', 3), 3),

    # Wrong arguments
    (('bar',), sievemgr.ShellUsageError),
    (('bar', 'foo',), ValueError),

    # Non-existing functions
    (('-',), sievemgr.ShellUsageError),
    (('do_',), sievemgr.ShellUsageError),
    (('do_foo',), sievemgr.ShellUsageError),
    (('frobnicate',), sievemgr.ShellUsageError),
    (('-', 'foo'), sievemgr.ShellUsageError),
    (('do_', 'foo'), sievemgr.ShellUsageError),
    (('do_foo', 'foo'), sievemgr.ShellUsageError),
    (('frobnicate', 'foo'), sievemgr.ShellUsageError),

    # Aliases
    (('!',), 0),
    (('!', 'foo',), 1),
    (('!', 'foo', 'bar',), 2),
    (('!', 'foo', 'bar', 'baz',), 3),
)


COMPLETIONS: Final[tuple[Test[Optional[str]], ...]] = (
    # Empty
    *[(('', '', i), cmd) for i, cmd in enumerate
      (('bar ', 'exit ', 'foo ', 'help ', 'sh ', None))],

    # Commands
    *[(('', cmd[0:i], j), cmd if j == 0 else None)
      for cmd in ('bar ', 'exit ', 'foo ', 'help ')
      for i in range(1, len(cmd))
      for j in (0, 1)],

    # Arguments
    (('exit ', '', 0), None),
    (('exit ', '', 1), None),
    (('help ', '', 0), 'bar '),
    (('help ', '', 1), 'exit '),
    (('help ', '', 2), 'foo '),
    (('help ', '', 3), 'help '),
    (('help ', '', 4), 'sh '),
    (('help ', '', 5), None),
    (('foo ', '', 0), 'bar '),
    (('foo ', '', 1), 'baz '),
    (('foo ', '', 2), 'foo '),
    (('foo ', '', 3), None),
    (('foo ', '', 4), None),
    (('foo ', 'b', 0), 'bar '),
    (('foo ', 'b', 1), 'baz '),
    (('foo ', 'b', 2), None),
    (('foo ', 'b', 3), None),
    (('foo ', 'ba', 0), 'bar '),
    (('foo ', 'ba', 1), 'baz '),
    (('foo ', 'ba', 2), None),
    (('foo ', 'ba', 3), None),
    (('foo ', 'bar', 0), 'bar '),
    (('foo ', 'bar', 1), None),
    (('foo ', 'bar', 2), None),
    (('foo ', 'baz', 0), 'baz '),
    (('foo ', 'baz', 1), None),
    (('foo ', 'baz', 2), None),
    (('foo ', 'bar ', 0), None),
    (('foo ', 'bar ', 1), None),
    (('foo ', 'baz ', 0), None),
    (('foo ', 'baz ', 1), None),
    (('foo ', 'f', 0), 'foo '),
    (('foo ', 'f', 1), None),
    (('foo ', 'f', 2), None),
    (('foo ', 'fo', 0), 'foo '),
    (('foo ', 'fo', 1), None),
    (('foo ', 'fo', 2), None),
    (('foo ', 'foo', 0), 'foo '),
    (('foo ', 'foo', 1), None),
    (('foo ', 'foo', 2), None),
    (('foo ', 'foo ', 0), None),
    (('foo ', 'foo ', 1), None),
    (('foo foo ', '', 0), 'quuuux'),
    (('foo foo ', '', 1), 'quuux'),
    (('foo foo ', '', 2), 'quux'),
    (('foo foo ', '', 3), None),
    (('foo foo ', 'q', 0), 'quuuux'),
    (('foo foo ', 'q', 1), 'quuux'),
    (('foo foo ', 'q', 2), 'quux'),
    (('foo foo ', 'q', 3), None),
    (('foo foo ', 'qu', 0), 'quuuux'),
    (('foo foo ', 'qu', 1), 'quuux'),
    (('foo foo ', 'qu', 2), 'quux'),
    (('foo foo ', 'qu', 3), None),
    (('foo foo ', 'quu', 0), 'quuuux'),
    (('foo foo ', 'quu', 1), 'quuux'),
    (('foo foo ', 'quu', 2), 'quux'),
    (('foo foo ', 'quu', 3), None),
    (('foo foo ', 'quuu', 0), 'quuuux'),
    (('foo foo ', 'quuu', 1), 'quuux'),
    (('foo foo ', 'quuu', 2), None),
    (('foo foo ', 'quuuu', 0), 'quuuux'),
    (('foo foo ', 'quuuu', 1), None),
    (('foo foo ', 'quuuuu', 0), None),
    (('foo foo ', 'quuuux', 0), 'quuuux'),
    (('foo foo ', 'quuuux', 1), None),
    (('foo foo ', 'quuuux ', 0), None),

    # Patterns
    (('foo ', '*', 0), 'bar '),
    (('foo ', '*', 1), 'baz '),
    (('foo ', '*', 2), 'foo '),
    (('foo ', '*', 3), None),
    (('foo ', 'f*', 0), 'foo '),
    (('foo ', 'f*', 1), None),
    (('foo ', 'fo*', 0), 'foo '),
    (('foo ', 'fo*', 1), None),
    (('foo ', 'f*o', 0), 'foo '),
    (('foo ', 'f*o', 1), None),
    (('foo ', 'fo?', 0), 'foo '),
    (('foo ', 'fo?', 1), None),
    (('foo ', 'f?o', 0), 'foo '),
    (('foo ', 'f?o', 1), None),
    (('foo ', 'fo[mno]', 0), 'foo '),
    (('foo ', 'fo[mno]', 1), None),
    (('foo ', 'f[o]o', 0), 'foo '),
    (('foo ', 'f[o]o', 1), None),
    (('foo ', 'f[np]o', 0), None)
)


CONFIRMATIONS: Final[tuple[Test[ConfirmEnum], ...]] = (
    # Defaults
    *[((' ' * i, enum), enum)
      for enum in ConfirmEnum
      for i in range(3)],

    # Yes, no, all, and none
    *[((z, ConfirmEnum.YES), ConfirmEnum.NO)
      for x in ('n', 'no')
      for y in mixcase(x)
      for z in addspaces(y)],
    *[((z, ConfirmEnum.NO), ConfirmEnum.YES)
      for x in ('y', 'yes')
      for y in mixcase(x)
      for z in addspaces(y)],
    *[((y, ConfirmEnum.YES, True), ConfirmEnum.NONE)
      for x in mixcase('none')
      for y in addspaces(x)],
    *[((y, ConfirmEnum.NO, True), ConfirmEnum.ALL)
      for x in mixcase('all')
      for y in addspaces(x)],

    # Errors
    *[((z, ConfirmEnum.NO), ValueError)
      for x in ('all', 'none')
      for y in mixcase(x)
      for z in addspaces(y)],
    *[((z, ConfirmEnum.NO, False), ValueError)
      for x in ('all', 'none')
      for y in mixcase(x)
      for z in addspaces(y)],
    *[((z, ConfirmEnum.NO), ValueError)
      for x in ('foo', 'bar', 'baz')
      for y in mixcase(x)
      for z in addspaces(y)],
    *[((z, ConfirmEnum.NO, False), ValueError)
      for x in ('foo', 'bar', 'baz')
      for y in mixcase(x)
      for z in addspaces(y)],
)


HELPMESSAGES: Final[tuple[Test[str], ...]] = (
    (('foo',), 'foo [arg ...] - returns number of arguments\n'),
    (('bar',), 'bar n - returns n\n'),
    (('help',), 'help [command] - list commands/show help for command\n'),
    (('sh',), 'sh [arg ...] - wraps foo\n'),
    (('qux',), sievemgr.ShellUsageError)
)


PATTERNSRAW: Final[tuple[Test[list[ShellWord]], ...]] = (
    (('* bar',), [ShellPattern('*'), 'bar']),
    (('? bar',), [ShellPattern('?'), 'bar']),
    (('[foo] bar',), [ShellPattern('[foo]'), 'bar']),
    ((r'\* bar',), [r'*', 'bar']),
    ((r'\? bar',), [r'?', 'bar']),
    ((r'\[foo] bar',), [r'[foo]', 'bar']),
    ((r'\** bar',), [ShellPattern(r'[*]*'), 'bar']),
    ((r'\?? bar',), [ShellPattern(r'[?]?'), 'bar']),
    ((r'\[foo][bar] baz',), [ShellPattern(r'[[]foo][bar]'), 'baz'])
)


PATTERNSEXP: Final[tuple[Test[list[ShellWord]], ...]] = (
    (('foo * ',), ['foo', 'bar', 'baz', 'foo']),
    (('foo ?oo',), ['foo', 'foo']),
    (('foo [f]oo',), ['foo', 'foo']),
    ((r'foo \*',), ['foo', '*']),
    ((r'foo \?',), ['foo', '?']),
    ((r'foo \[foo]',), ['foo', '[foo]']),
    ((r'foo \**',), sievemgr.ShellOperationError),
    ((r'foo \??',), sievemgr.ShellOperationError),
    ((r'foo \[foo][foo]',), sievemgr.ShellOperationError)
)


SCRIPTS: Final[tuple[Test[int], ...]] = (
    # Empty
    ((StringIO(""""""),), 0),
    ((StringIO("""
               """),), 0),
    ((StringIO("""
               # This is a no-op
               """),), 0),
    # Simple
    ((StringIO("""
               # This is a comment
               foo
               """),), 0),
    ((StringIO("""
               # This is a comment
               bar 0
               """),), 0),
    ((StringIO("""
               # This is a comment
               bar 0
               foo
               """),), 0),
    ((StringIO("""
               # This is a comment
               foo
               bar 0
               """),), 0),
    ((StringIO("""
               exit
               # Not reached
               bar 77
               """),), 0),

    # Non-zero return values
    ((StringIO("""
               foo bar
               # Not reached
               bar 0
               """),), 1),
    ((StringIO("""
               foo bar baz
               # Not reached
               bar 99
               """),), 2),
    ((StringIO("""
               bar 77
               # Not reached
               bar 99
               """),), 77),

    # Exceptions
    ((StringIO("""
               foo 'Unbalanced quote
               # Not reached
               bar 77
               """),), sievemgr.ShellDataError),
    ((StringIO("""
               no such command
               # Not reached
               bar 77
               """),), sievemgr.ShellUsageError),
    ((StringIO("""
               bar does not take that many arguments
               # Not reached
               bar 77
               """),), sievemgr.ShellUsageError),

) + tuple(((StringIO(s),), r) for ((s,), r) in COMMANDLINES)


# pylint: disable=inconsistent-quotes
SPLITLINES: Final[tuple[Test[list[ShellWord]], ...]] = (
    # Simple
    (('foo',), ['foo']),
    (('foo bar',), ['foo', 'bar']),
    (('foo bar baz',), ['foo', 'bar', 'baz']),

    # Empty
    (('',), []),
    ((' ',), []),
    (('        ',), []),
    (('\t',), []),
    (('\t\n     ',), []),
    (('#',), []),
    (('# foo',), []),
    (('    # foo',), []),
    (('\t\n    # foo\\\nfoo',), []),

    # Leading and trailing whitespace
    ((' foo',), ['foo']),
    ((' foo bar',), ['foo', 'bar']),
    ((' foo bar baz',), ['foo', 'bar', 'baz']),
    (('  foo',), ['foo']),
    (('  foo bar',), ['foo', 'bar']),
    (('  foo bar baz',), ['foo', 'bar', 'baz']),
    (('      foo',), ['foo']),
    (('      foo bar',), ['foo', 'bar']),
    (('      foo bar baz',), ['foo', 'bar', 'baz']),
    (('\tfoo',), ['foo']),
    (('\tfoo bar',), ['foo', 'bar']),
    (('\tfoo bar baz',), ['foo', 'bar', 'baz']),
    (('\t\tfoo',), ['foo']),
    (('\t\tfoo bar',), ['foo', 'bar']),
    (('\t\tfoo bar baz',), ['foo', 'bar', 'baz']),
    (('\t\t\t\t\t\tfoo',), ['foo']),
    (('\t\t\t\t\t\tfoo bar',), ['foo', 'bar']),
    (('\t\t\t\t\t\tfoo bar baz',), ['foo', 'bar', 'baz']),
    (('\nfoo',), ['foo']),
    (('\nfoo bar',), ['foo', 'bar']),
    (('\nfoo bar baz',), ['foo', 'bar', 'baz']),
    (('\n\nfoo',), ['foo']),
    (('\n\nfoo bar',), ['foo', 'bar']),
    (('\n\nfoo bar baz',), ['foo', 'bar', 'baz']),
    (('\n\n\n\n\nfoo',), ['foo']),
    (('\n\n\n\n\nfoo bar',), ['foo', 'bar']),
    (('\n\n\n\n\nfoo bar baz',), ['foo', 'bar', 'baz']),
    (('\n\t  \n\n \tfoo bar baz',), ['foo', 'bar', 'baz']),
    (('foo ',), ['foo']),
    (('foo bar ',), ['foo', 'bar']),
    (('foo bar baz ',), ['foo', 'bar', 'baz']),
    (('foo  ',), ['foo']),
    (('foo bar  ',), ['foo', 'bar']),
    (('foo bar baz  ',), ['foo', 'bar', 'baz']),
    (('foo      ',), ['foo']),
    (('foo bar      ',), ['foo', 'bar']),
    (('foo bar baz      ',), ['foo', 'bar', 'baz']),
    (('foo\t',), ['foo']),
    (('foo bar\t',), ['foo', 'bar']),
    (('foo bar baz\t',), ['foo', 'bar', 'baz']),
    (('foo\t\t',), ['foo']),
    (('foo bar\t\t',), ['foo', 'bar']),
    (('foo bar baz\t\t',), ['foo', 'bar', 'baz']),
    (('foo\t\t\t\t\t\t',), ['foo']),
    (('foo bar\t\t\t\t\t\t',), ['foo', 'bar']),
    (('foo bar baz\t\t\t\t\t\t',), ['foo', 'bar', 'baz']),
    (('foo\n',), ['foo']),
    (('foo bar\n',), ['foo', 'bar']),
    (('foo bar baz\n',), ['foo', 'bar', 'baz']),
    (('foo\n\n',), ['foo']),
    (('foo bar\n\n',), ['foo', 'bar']),
    (('foo bar baz\n\n',), ['foo', 'bar', 'baz']),
    (('foo\n\n\n\n\n',), ['foo']),
    (('foo bar\n\n\n\n\n',), ['foo', 'bar']),
    (('foo bar baz\n\n\n\n\n',), ['foo', 'bar', 'baz']),
    (('foo bar baz\n\t  \n\n \t',), ['foo', 'bar', 'baz']),
    (('foo\n\t \n\t',), ['foo']),
    (('foo bar\n\t \t',), ['foo', 'bar']),
    (('foo bar baz\t\n \t\n',), ['foo', 'bar', 'baz']),
    (('foo  bar',), ['foo', 'bar']),
    (('foo  bar  baz',), ['foo', 'bar', 'baz']),
    (('foo     bar',), ['foo', 'bar']),
    (('foo     bar       baz',), ['foo', 'bar', 'baz']),
    (('foo\tbar',), ['foo', 'bar']),
    (('foo\t\tbar',), ['foo', 'bar']),
    (('foo\t\tbar\t\tbaz',), ['foo', 'bar', 'baz']),
    (('foo\t\t\t\tbar',), ['foo', 'bar']),
    (('foo\t\t\t\tbar\t\t\t\tbaz',), ['foo', 'bar', 'baz']),
    (('foo\nbar',), ['foo', 'bar']),
    (('foo\n\nbar',), ['foo', 'bar']),
    (('foo\n\nbar\n\nbaz',), ['foo', 'bar', 'baz']),
    (('foo\n\n\n\nbar',), ['foo', 'bar']),
    (('foo\n\n\n\nbar\n\n\n\nbaz',), ['foo', 'bar', 'baz']),
    (('foo\n \tbar',), ['foo', 'bar']),
    (('foo\n \t\t\n   bar',), ['foo', 'bar']),
    (('foo\n\t\nbar\n  \t\nbaz',), ['foo', 'bar', 'baz']),
    (('foo\n  \t\n\n\nbar',), ['foo', 'bar']),
    (('foo\n\n\n\nbar\n  \t\n \n\nbaz',), ['foo', 'bar', 'baz']),
    (('  foo\n \tbar\n\n\n',), ['foo', 'bar']),
    ((' \t  \tfoo\n \t\t\n   bar   ',), ['foo', 'bar']),
    (('  \n\t\tfoo\n\t\nbar\n  \t\nbaz\n',), ['foo', 'bar', 'baz']),
    (('  foo\n  \t\n\n\nbar\t  \t  \t\n',), ['foo', 'bar']),
    (('\tfoo\n\n\n\nbar\n  \t\n \n\nbaz',), ['foo', 'bar', 'baz']),

    # Quotes
    (('"foo bar"',), ["foo bar"]),
    (("'foo bar'",), ["foo bar"]),
    (("\"\"\"\"",), ['']),
    (("\\foo",), ["foo"]),
    (('"foo\\bar"',), ['foo\\bar']),
    (('"foo\\\\bar"',), ['foo\\bar']),
    (('"foo\'bar"',), ["foo'bar"]),
    (("'foo\"bar'",), ['foo"bar']),
    (("'\"\\\"\\\\ \n\t'",), ["\"\\\"\\\\ \n\t"]),
    (("\"'foo\" \"bar'\"",), ["'foo", "bar'"]),
    (("\"'foo' 'bar'\"",), ["'foo' 'bar'"]),
    (("'\"foo\" \"bar\"'",), ["\"foo\" \"bar\""]),
    (("'foo\\'bar'",), ShellDataError),
    # pylint: disable=line-too-long
    (("'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"foo\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'\"'",), ["\"'\"'\"'\"'\"'\"'\"'\"'foo'\"'\"'\"'\"'\"'\"'\"'\""]),    # noqa

    # Escaped meta-characters
    (("\\\"",), ["\""]),
    (("\\'",), ["'"]),
    (("\\\\",), ["\\"]),
    (("\\\\\"",), ShellDataError),
    (("\\\\'",), ShellDataError),
    (("\\\\\\",), ["\\"]),
    (("foo\\\"",), ["foo\""]),
    (("foo\\'",), ["foo'"]),
    (("foo\\\\",), ["foo\\"]),
    (("foo\\\\\"",), ShellDataError),
    (("foo\\\\'",), ShellDataError),
    (("foo\\\\\\",), ["foo\\"]),
    (("\\\"foo",), ["\"foo"]),
    (("\\'foo",), ["'foo"]),
    (("\\\\foo",), ["\\foo"]),
    (("\\\\\"foo",), ShellDataError),
    (("\\\\'foo",), ShellDataError),
    (("\\\\\\foo",), ["\\foo"]),
    (("foo\\\"foo",), ["foo\"foo"]),
    (("foo\\'foo",), ["foo'foo"]),
    (("foo\\\\foo",), ["foo\\foo"]),
    (("foo\\\\\"foo",), ShellDataError),
    (("foo\\\\'foo",), ShellDataError),
    (("foo\\\\\\foo",), ["foo\\foo"]),
    (("foo\\ bar",), ["foo bar"]),
    (("foo\\\nbar",), ["foo\nbar"]),
    (("foo\\\tbar",), ["foo\tbar"]),
    ((r'foo\ bar',), ['foo bar']),

    # Escaped and quoted patterns
    ((r'\*',), ['*']),
    ((r'\?',), ['?']),
    ((r'\[',), ['[']),
    ((r'"*"',), ['*']),
    ((r'"?"',), ['?']),
    ((r'"["',), ['[']),
    ((r"'*'",), ['*']),
    ((r"'?'",), ['?']),
    ((r"'['",), ['[']),
    ((r'foo \*',), ['foo', '*']),
    ((r'\?\bar',), ['?bar']),
    ((r'\[foo]',), ['[foo]']),
    ((r'"foo *"',), ['foo *']),
    ((r'"b?r"',), ['b?r']),
    ((r'"[bar" ]',), ['[bar', ']']),
    ((r"'fo*' fo",), ['fo*', 'fo']),
    ((r"bar '?'",), ['bar', '?']),
    ((r"'['",), ['[']),

    # Quoted escapes
    (("\"\\\"\"",), ["\""]),
    (("\"\\'\"",), ["\\'"]),
    (("\"\\\\\"",), ["\\"]),
    (("\"\\\\\"\"",), ShellDataError),
    (("\"\\\\'\"",), ["\\'"]),
    (("\"\\\\\\\"",), ShellDataError),
    (("\"foo\\\"\"",), ["foo\""]),
    (("\"foo\\'\"",), ["foo\\'"]),
    (("\"foo\\\\\"",), ["foo\\"]),
    (("\"foo\\\\\"\"",), ShellDataError),
    (("\"foo\\\\'\"",), ["foo\\'"]),
    (("\"foo\\\\\\\"",), ShellDataError),
    (("\"\\\"foo\"",), ["\"foo"]),
    (("\"\\'foo\"",), ["\\'foo"]),
    (("\"\\\\foo\"",), ["\\foo"]),
    (("\"\\\\\"foo\"",), ShellDataError),
    (("\"\\\\'foo\"",), ["\\'foo"]),
    (("\"\\\\\\foo\"",), ["\\\\foo"]),
    (("\"foo\\\"foo\"",), ["foo\"foo"]),
    (("\"foo\\'foo\"",), ["foo\\'foo"]),
    (("\"foo\\\\foo\"",), ["foo\\foo"]),
    (("\"foo\\\\\"foo\"",), ShellDataError),
    (("\"foo\\\\'foo\"",), ["foo\\'foo"]),
    (("\"foo\\\\\\foo\"",), ["foo\\\\foo"]),
    (("\"foo\\ bar\"",), ["foo\\ bar"]),
    (("\"foo\\\nbar\"",), ["foo\nbar"]),
    (("\"foo\\\tbar\"",), ["foo\\\tbar"]),
    (("'\\\"'",), ["\\\""]),
    (("'\\''",), ShellDataError),
    (("'\\\\'",), ["\\\\"]),
    (("'\\\\\"'",), ["\\\\\""]),
    (("'\\\\''",), ShellDataError),
    (("'\\\\\\'",), ["\\\\\\"]),
    (("'foo\\\"'",), ["foo\\\""]),
    (("'foo\\''",), ShellDataError),
    (("'foo\\\\'",), ["foo\\\\"]),
    (("'foo\\\\\"'",), ["foo\\\\\""]),
    (("'foo\\\\''",), ShellDataError),
    (("'foo\\\\\\'",), ["foo\\\\\\"]),
    (("'\\\"foo'",), ["\\\"foo"]),
    (("'\\'foo'",), ShellDataError),
    (("'\\\\foo'",), ["\\\\foo"]),
    (("'\\\\\"foo'",), ["\\\\\"foo"]),
    (("'\\\\'foo'",), ShellDataError),
    (("'\\\\\\foo'",), ["\\\\\\foo"]),
    (("'foo\\\"foo'",), ["foo\\\"foo"]),
    (("'foo\\'foo'",), ShellDataError),
    (("'foo\\\\foo'",), ["foo\\\\foo"]),
    (("'foo\\\\\"foo'",), ["foo\\\\\"foo"]),
    (("'foo\\\\'foo'",), ShellDataError),
    (("'foo\\\\\\foo'",), ["foo\\\\\\foo"]),
    (("'foo\\ bar'",), ["foo\\ bar"]),
    (("'foo\\\nbar'",), ["foo\\\nbar"]),
    (("'foo\\\tbar'",), ["foo\\\tbar"]),

    # Trailing escapes
    (('\\',), []),
    (('\t\\',), []),
    (('""\\',), ['']),
    (('foo\\',), ['foo']),
)


USAGE: Final[tuple[Test[Optional[str]], ...]] = (
    ((None,), None),
    (('',), None),
    (('-',), None),
    ((' -',), None),
    (('- ',), None),
    ((' - ',), None),
    (('- foo',), None),
    (('-- foo',), 'usage: -'),
    (('frob',), 'usage: frob'),
    (('frob foo - frobnicate foo',), 'usage: frob foo'),
    (('frob [-o] foo - frobnicate foo',), 'usage: frob [-o] foo'),
)


WORDS: Final[tuple[Test[str], ...]] = (
    (((), 0), ''),
    ((('',), 0), '\n'),
    (((' ',), 0), '\n'),
    (((), 1), ''),
    ((('',), 1), '\n'),
    (((' ',), 1), '\n'),
    (((), 2), ''),
    ((('',), 2), '\n'),
    (((' ',), 2), '\n'),
    ((('foo',), 3), 'foo\n'),
    ((('foo', 'bar'), 3), 'foo\nbar\n'),
    ((('foo', 'bar'), 4), 'foo\nbar\n'),
    ((('foo', 'bar'), 6), 'foo\nbar\n'),
    ((('foo', 'bar'), 7), 'foo bar\n')
)


#
# Classes
#

class MockReadline():
    def __init__(self, line: str):
        for func in self.functions:
            self.mockups[func] = patch(func, autospec=True)
        self.line = line

    def __enter__(self) -> 'MockReadline':
        ctx = {func.removeprefix('readline.'): mockup.__enter__()
               for func, mockup in self.mockups.items()}
        ctx['get_begidx'].return_value = None
        ctx['get_line_buffer'].return_value = self.line
        return self

    def __exit__(self, exctype, excvalue, traceback):
        for mockup in self.mockups.values():
            mockup.__exit__(exctype, excvalue, traceback)

    functions = ('readline.get_begidx', 'readline.get_line_buffer')
    mockups: dict = {}


class TestShellMixin():
    def do_foo(self, *args):
        """foo [arg ...] - returns number of arguments"""
        return len(args)

    def do_bar(self, n):
        """bar n - returns n"""
        return int(n)

    def do_sh(self, *args):
        """sh [arg ...] - wraps foo"""
        return self.do_foo(*args)

    def do_(self):
        pass

    def complete_foo(self, argidx, _) -> tuple[tuple[str, bool], ...]:
        if argidx == 0:
            return (('impossible', False),)
        if argidx == 1:
            return tuple((s, True) for s in ('foo', 'bar', 'baz'))
        if argidx == 2:
            return tuple((s, False) for s in ('quux', 'quuux', 'quuuux'))
        return ()


#
# Tests
#

class TestBaseShell(unittest.TestCase):
    # Wrappers
    @classmethod
    def columnize(cls, words: Sequence[str],
                  width: int = shutil.get_terminal_size().columns) -> str:
        buf = io.StringIO()
        cls.basecls.columnize(words, file=buf, width=width)
        return buf.getvalue()

    def complete(self, line: str, text: str, n: int) -> Optional[str]:
        with MockReadline(line=line):
            return self.shell.complete(text, n)

    @classmethod
    def confirm(cls, line: str, default: ConfirmEnum = ConfirmEnum.NO,
                multi: bool = False) -> ConfirmEnum:
        confirm = cls.basecls.confirm
        with patch('sievemgr.TermIO.readline', autospec=True) as readline:
            with patch('sievemgr.TermIO.write', autospec=True):
                readline.return_value = line
                return confirm('', default=default, multi=multi, attempts=1)

    def expand(self, line: str) -> list[ShellWord]:
        return self.shell.expand(line)

    @classmethod
    def _getargs(cls, line: str) -> list[ShellWord]:
        with MockReadline(line=line):
            # pylint: disable=protected-access
            return cls.basecls._getargs()

    @classmethod
    def getusage(cls, docstring: Optional[str]) -> Optional[str]:
        def func():
            pass
        func.__doc__ = docstring
        return cls.basecls.getusage(func)

    @classmethod
    def split(cls, line: str) -> list[ShellWord]:
        return cls.basecls.split(line)

    # Setup
    def setUp(self):
        self.shellcls = type('TestShell', (self.basecls, TestShellMixin), {})
        self.shell = self.shellcls()

    # Tests
    def test_init(self):
        base = self.basecls()
        assert {'exit', 'help'} <= set(base.commands)
        sh = self.shellcls()
        assert {'bar', 'exit', 'foo', 'help'} <= set(sh.commands)

    def test_columnize(self):
        return runtests(self, self.columnize, WORDS)

    def test_complete(self):
        return runtests(self, self.complete, COMPLETIONS)

    @unittest.skipIf(os.getenv('QUICKTEST'), 'slow')
    def test_confirm(self):
        self.assertRaises(AssertionError, self.basecls.confirm,
                          'foo', attempts=0)
        return runtests(self, self.confirm, CONFIRMATIONS)

    def test_enter(self):
        for i, (args, exp) in enumerate(SCRIPTS):
            with self.subTest(i=i, args=args, exp=exp):
                try:
                    buffer, = args
                except ValueError:
                    continue
                buffer.seek(0)
                oldstdin = sys.stdin
                sys.stdin = buffer
                try:
                    retval = self.shell.enter()
                # pylint: disable=broad-exception-caught
                except Exception as err:
                    if not isinstance(exp, type):
                        raise
                    self.assertIsInstance(err, exp)
                    continue
                finally:
                    sys.stdin = oldstdin
                    buffer.seek(0)
                # pylint: disable=bad-indentation
                if (isinstance(exp, type)
                    and issubclass(exp, sievemgr.ShellError)):
                        self.assertNotEqual(retval, 0)
                else:
                    self.assertEqual(retval, exp)

    def test_execute(self):
        return runtests(self, self.shell.execute, COMMANDS)

    def test_executeline(self):
        return runtests(self, self.shell.executeline, COMMANDLINES)

    def test_executescript(self):
        def rewind(file):
            with contextlib.suppress(AttributeError, io.UnsupportedOperation):
                file.seek(0)
        walk(SCRIPTS, rewind)
        try:
            return runtests(self, self.shell.executescript, SCRIPTS)
        finally:
            walk(SCRIPTS, rewind)

    def test_expand(self):
        return runtests(self, self.expand, SPLITLINES + PATTERNSEXP)

    def test_getargs(self):
        return runtests(self, self._getargs, SPLITLINES + PATTERNSRAW)

    def test_getcommands(self):
        self.assertEqual(list(self.shell.getcommands()),
                         ['bar', 'exit', 'foo', 'help', 'sh'])

    def test_getprompt(self):
        self.assertEqual(self.shell.getprompt(), '> ')

    def test_getusage(self):
        return runtests(self, self.getusage, USAGE)

    def test_hasatty(self):
        oldstdin = sys.stdin
        sys.stdin = StringIO()
        try:
            self.assertEqual(self.shell.hasatty(), False)
        finally:
            sys.stdin = oldstdin
        try:
            with sievemgr.TermIO() as terminal:
                sys.stdin = terminal
                try:
                    self.assertEqual(self.shell.hasatty(), True)
                finally:
                    sys.stdin = oldstdin
        except FileNotFoundError:
            self.skipTest('Terminal is not a teletype device')

    @unittest.skipIf(os.getenv('QUICKTEST'), 'slow')
    def test_split(self):
        return runtests(self, self.split, SPLITLINES + AUTOWORDS + PATTERNSRAW)

    def test_do_exit(self):
        try:
            self.shell.do_exit()
        except StopIteration:
            return
        self.fail('expected StopIteration')

    def test_do_help(self):
        for i, (args, exp) in enumerate(HELPMESSAGES):
            with self.subTest(i=i, args=args, exp=exp):
                buffer = StringIO()
                buffer.seek(0)
                oldstdout = sys.stdout
                sys.stdout = buffer
                try:
                    self.shell.do_help(*args)
                # pylint: disable=broad-exception-caught
                except Exception as err:
                    if not isinstance(exp, type):
                        raise
                    self.assertIsInstance(err, exp)
                    continue
                finally:
                    sys.stdout = oldstdout
                    buffer.seek(0)
                self.assertEqual(buffer.getvalue(), exp)

    basecls: type[BaseShell] = BaseShell

    shellcls: type[BaseShell]


#
# Boilerplate
#

if __name__ == '__main__':
    unittest.main()
