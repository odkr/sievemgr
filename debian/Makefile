#
# Special targets
#

.PHONY: \
	all \
	clean \
	init \
	install \
	packaging-tools \
	post-install \
	tidy \
	upload


#
# General macros
#

GPG = gpg
GPGFLAGS = -q --batch --yes

package = sievemgr
version = $(shell python3 ../sievemgr.py --version | awk 'NR == 1 {print $$2}')
maintname = $(shell git config --get user.name)
maintmail = $(shell git config --get user.email)


#
# Pattern rules
#

%.deb.asc: %.deb
	$(GPG) -ab $(GPGFLAGS) $<


#
# Build
#

DEBSIGS = debsigs
DPKG-DEB = dpkg-deb
GZIP = gzip
LINTIAN = lintian
LINTIANFLAGS = --cfg lintian.cfg --fail-on=error,pedantic,warning

arch = all
builddir = build-$(arch)
debiandir = $(dpkgdir)/DEBIAN
dpkg = $(package)-$(version)_0-$(arch).deb
dpkgdir = $(dpkg:.deb=)
docdir = $(dpkgdir)/usr/share/doc/$(package)
mandir = $(dpkgdir)/usr/share/man
instlog = /dev/null

all: $(dpkg) $(dpkg).asc

$(dpkg): install $(debiandir)/control $(docdir)/copyright $(docdir)/changelog.gz
	if [ -e $(dpkgdir)/usr/local ]; then \
		mv $(dpkgdir)/usr/local/* $(dpkgdir)/usr; \
		rmdir $(dpkgdir)/usr/local; \
	fi
	pydir="$$(ls -d $(dpkgdir)/usr/lib/python*)"; \
	debpydir="$${pydir%.*}"; \
	mv "$$pydir" "$$debpydir"; \
	if [ -e "$$debpydir/site-packages" ]; \
	then mv "$$debpydir/site-packages" "$$debpydir/dist-packages"; \
	fi
	find $(dpkgdir) -type d -name __pycache__ -exec rm -rf '{}' ';' -prune
	find $(mandir) -type f -name '*.[123456789]' -exec $(GZIP) -9fn '{}' +
	chmod -R go-w $(dpkgdir)
	$(DPKG-DEB) $(DPKG-DEBFLAGS) --root-owner-group --build $(dpkgdir)
	$(LINTIAN) $(LINTIANFLAGS) $(dpkg) || [ $$? -eq 127 ]
	GPG_TTY=$$(tty) $(DEBSIGS) $(DEBSIGSFLAGS) --sign=maint $(dpkg)

install:
	cd .. && $(MAKE) -e \
		DESTDIR=debian/$(dpkgdir) \
		PREFIX=/usr \
		PIPFLAGS='--root=debian/$(dpkgdir) --prefix=/usr --no-deps' \
		instlog=$(instlog) \
		zshcompdir=/usr/share/zsh/vendor-completions \
		install

$(debiandir)/control: control.in
	dirname $@ | xargs mkdir -pm0755
	sed "\
		s/ARCH/$(arch)/; \
		s/VERSION/$(version)/; \
		s/NAME/$(maintname)/; \
		s/MAIL/$(maintmail)/; \
	" control.in >$@

$(docdir)/copyright: copyright
	dirname $@ | xargs mkdir -pm0755
	cp copyright $@

$(docdir)/changelog.gz: ../NEWS.rst
	dirname $@ | xargs mkdir -pm0755
	$(GZIP) -9n <../NEWS.rst >$@


#
# Tools
#

init: packaging-tools

packaging-tools:
	sudo apt-get install debsigs dpkg gpg lintian python3-pip


#
# Upload
#

baseurl = https://codeberg.org/api/packages/$(uploaduser)
debrelease = stable
uploadurl = $(baseurl)/debian/pool/$(debrelease)/main/upload
uploaduser = odkr

upload: all
	GPG_TTY=$$(tty) $(DEBSIGS) $(DEBSIGSFLAGS) --sign=origin *.deb
	for pkg in *.deb *.deb.asc; \
	do curl -nT"$$pkg" $(uploadurl); \
	done


#
# Cleanup
#

tidy:
	find . -type d '(' -name 'build-*' -o -name '$(package)-*' ')' \
		-exec rm -rf '{}' +

clean: tidy
	rm -f *.asc *.deb
