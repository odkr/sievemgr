**************
Debian package
**************

.. highlight:: none

The :file:`debian` sub-directory contains instructions
for building a Debian package.


Requirements
============

The :file:`Makefile` requires `GNU Make`_, which is the default Make
under Debian and will be installed automatically if you follow the
instructions below.


Preparation
===========

Install build and packaging tools::

    sudo apt-get install debsigs dpkg gpg lintian python3-pip


Build
=====

Build and sign the package::

    make

