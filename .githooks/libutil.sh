#!/bin/sh

#
# Shell utility functions
#
# Copyright 2022-2024 Odin Kroeger.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#

# shellcheck disable=2015,2031

#
# Save the caught signal to $caught.
# If $catch is non-empty, also call atexit and reraise the signal.
#
# Usage:
#	catch signal
#
# Globals:
#	$catch   Whether to call atexit and reraise the signal.
#	$caught	 Set to caught signal.
#
catch() {
	: "${1:?}"

	warn 'Caught %s' "$1"
	caught="$1"
	if [ "${catch-}" ]
	then
		atexit
		trap - "$1"
		kill -s "$1" "$$"
	fi
}


#
# Disable signal handlers, terminate all jobs and the process group,
# run and clear $atexit, and return $?.
#
# Usage:
#	atexit
#
# Globals:
#	$atexit	         Code to be eval-ed.
#	$_atexit_retval  Return value.
#
atexit() {
	# shellcheck disable=2319
	_atexit_retval=$?
	set +e
	trap '' EXIT ALRM HUP INT TERM USR1 USR2
	_atexit_jobs="$(jobs -p 2>/dev/null)"
	# shellcheck disable=2086
	kill -s TERM $_atexit_jobs -$$ >/dev/null 2>&1
	# shellcheck disable=2086
	kill -s CONT $_atexit_jobs -$$ >/dev/null 2>&1
	unset _atexit_jobs
	eval "${atexit-}"
	atexit=
	wait
	return $_atexit_retval
}


#
# Clear the current line of the given file descriptor (default: 2),
# but only if the file is a teletype device.
#
# Usage:
#	clearln [fd]
#
clearln() (
	_clearln_fd="${1:-2}"
	if [ -t "$_clearln_fd" ]
	then printf '\033[0K' >&"$_clearln_fd"
	fi
)


#
# Print a message to standard error and exit.
#
# Usage:
#	err [-s status] format [argument ...]
#
# Options:
#	-s status  Exit with status (default: 1).
#
err() {
	_err_status=1
	OPTIND=1 OPTARG='' _err_opt=''
	while getopts s: _err_opt
	do
		case $_err_opt in
		(s) _err_status="${OPTARG:?}" ;;
		(*) exit 2
		esac
	done
	unset _err_opt
	shift $((OPTIND - 1))
	warn -c31 -- "$@"
	exit "$_err_status"
}


#
# Enforce POSIX-compliance, register signal handlers, and set globals.
#
# Usage:
#	init
#
# Globals:
#	$BIN_SH           Set to xpg4.
#	$CLICOLOR_FORCE   Set to the empty string.
#	$IFS              Unset.
#	$NULLCMD          Set to :.
#	$POSIXLY_CORRECT  Set to y.
#	$catch            Set to y.
#	$caught           Set to the empty string.
#	$colors           Number of colors supported by terminal.
#	$progname         Set to the basename of $0.
#	$quiet            -- " --.
#	$verbose          -- " --.
#
init() {
	set +e
	if [ "${BASH_VERSION-}" ] || [ "${KSH_VERSION-}" ]
	then
		# shellcheck disable=3040
		if ( set -o posix ) 2>/dev/null
		then set -o posix
		fi
	elif [ "${YASH_VERSION-}" ]
	then
		# shellcheck disable=3040
		if ( set -o posixly-correct ) 2>/dev/null
		then set -o posixly-correct
		fi
	elif [ "${ZSH_VERSION-}" ]
	then
		emulate sh               2>/dev/null
		setopt POSIX_ALIASES     2>/dev/null
		setopt POSIX_ARGZERO     2>/dev/null
		setopt POSIX_BUILTINS    2>/dev/null
		setopt POSIX_CD          2>/dev/null
		setopt POSIX_IDENTIFIERS 2>/dev/null
		setopt POSIX_JOBS        2>/dev/null
		setopt POSIX_STRINGS     2>/dev/null
		setopt POSIX_TRAPS       2>/dev/null
	fi
	set -e

	export BIN_SH=xpg4 NULLCMD=: POSIXLY_CORRECT=y CLICOLOR_FORCE=

	# Make sure IFS is safe
	unset IFS

	# Trap signals that would terminate the script
	catch='' caught=''
	# shellcheck disable=2064
	for _init_sig in ALRM HUP INT TERM USR1 USR2
	do trap "catch $_init_sig" "$_init_sig"
	done
	unset _init_sig

	trap atexit EXIT
	catch=y
	[ "$caught" ] && kill -s "$caught" "$$"

	# Safe permission mask
	umask 022

	# Output control
	quiet='' verbose=''

	[ "${CLICOLOR-}" ] && colors="$(tput colors 2>/dev/null)"
	: "${colors:=0}"

	# Program name
	progname="$(basename -- "$0")" || progname="${0##*/}"
	readonly progname
}


#
# Check if $2 matches any member of $@ using operator $1.
#
# Usage:
#	inlist operator needle [straw ...]
#
inlist() (
	# shellcheck disable=2034
	_inlist_op="${1:?}"
	_inlist_needle="${2?}"
	shift 2

	# shellcheck disable=2034
	for _inlist_straw
	do test "$_inlist_straw" "$_inlist_op" "$_inlist_needle" && return
	done

	return 1
)


#
# Create a lock file.
#
# Usage:
#	lock [-t n] file
#
# Options:
#	-t n  Wait n seconds to acquire lock file (default: 3).
#
lock() (
	OPTARG='' OPTIND=1 _lock_opt=''
	while getopts 't:' _lock_opt
	do
		case $_lock_opt in
		(t) _lock_timeout="$OPTARG" ;;
		(*) exit 2
		esac
	done
	shift $((OPTIND - 1))
	unset _lock_opt

	: "${1:?}"
	: "${_lock_timeout:=3}"
	_lock_file="$1"

	while [ "$_lock_timeout" -gt 0 ]
	do
		if ( printf '%d\n' "$$" >"$_lock_file"; ) >/dev/null 2>&1
		then
			return
		else
			if	! read -r _lock_pid <"$_lock_file"        ||
				! [ "$_lock_pid" ]                        ||
				! kill -0 -- "$_lock_pid" >/dev/null 2>&1
			then
				warn -q 'Removing %s...' "$_lock_file"
				rm -f "$_lock_file"
			elif	[ "$_lock_timeout" -gt 0 ]
			then
				sleep 1
			fi
		fi
		_lock_timeout=$((_lock_timeout - 1))
	done

	warn -c31 'Could not acquire %s' "$_lock_file"
	return 1
)


#
# Run $@ and redirect its output to a file unless $verbose is set.
#
# Usage:
#	logged command [argument ...]
#
# Options:
#	-d dir     Store log file in dir (default: .).
#	-i status  Do not save output if $@ exits with status (default: 0).
#	-l file    Store output in file (default: basename of $1).
#
#	-i can be given multiple times.
#
logged() (
	OPTIND=1 OPTARG='' _logged_opt=''
	while getopts 'd:i:l:' _logged_opt
	do
		case $_logged_opt in
		(d) _logged_dir="${OPTARG:?}" ;;
		(i) _logged_mask="$_logged_mask${OPTARG:?} " ;;
		(l) _logged_fname="${OPTARG:?}" ;;
		(*) return 1
		esac
	done
	shift $((OPTIND - 1))

	: "${1:?}"
	: "${_logged_dir:=.}"
	: "${_logged_fname:=}"
	: "${_logged_mask:=0}"

	if [ "$verbose" ]
	then
		set +e
		"$@"
		return
	fi

	# shellcheck disable=2030
	: "${TMPDIR:=/tmp}"
	: "${_logged_fname:="$(basename "$1").log"}"
	: "${_logged_fname:?}"
	_logged_log="$TMPDIR/$_logged_fname"

	set +e
	"$@" >>"$_logged_log" 2>&1
	_logged_xstatus=$?
	set -e

	# shellcheck disable=2086
	if inlist -eq "$_logged_xstatus" $_logged_mask
	then
		rm -f "$TMPDIR/$_logged_fname" >/dev/null 2>&1
	else
		warn -c31 '%s: Exited with status %d' "$1" "$_logged_xstatus"
		if mv "$_logged_log" "$_logged_dir" 2>/dev/null
		then warn -qc31 'See %s for details' "$_logged_fname"
		fi
	fi

	return $_logged_xstatus
)


#
# Create a temporary directory with the filename $1-$$ in $2,
# register it for deletion via $atexit, and set it as $TMPDIR.
#
# Globals:
#	$TMPDIR  Set to the created directory.
#	$atexit  Updated to reigster deletion of temporary directory.
#
mktmpdir() {
	[ "${_mktmpdir_tmpdir-}" ] && return

	OPTIND=1 OPTARG='' _mktmpdir_opt=''
	while getopts 'p:d:' _mktmpdir_opt
	do
		case $_mktmpdir_opt in
		(d) _mktmpdir_dir="$OPTARG" ;;
		(p) _mktmpdir_prefix="$OPTARG" ;;
		(*) exit 2
		esac
	done
	shift $((OPTIND - 1))
	unset _mktmpdir_opt

	: "${_mktmpdir_dir:="${TMPDIR:-/tmp}"}"
	: "${_mktmpdir_prefix:=tmp}"

	catch=
	readonly _mktmpdir_tmpdir="$_mktmpdir_dir/$_mktmpdir_prefix-$$"
	mkdir -m 0755 "$_mktmpdir_tmpdir" || exit
	atexit="rm -rf \"\$_mktmpdir_tmpdir\"; ${atexit-:}"
	catch=y
	[ "${caught-}" ] && kill -s "$caught" "$$"
	unset _mktmpdir_dir _mktmpdir_prefix

	export TMPDIR="$_mktmpdir_tmpdir"
}


#
# Return to the beginning of the current line of the given file
# descriptor (default: 2), but only if the file is a teletype device.
#
# Usage:
#	rewindln [fd]
#
rewindln() (
	_rewindln_fd="${1:-2}"
	if [ -t "$_rewindln_fd" ]
	then printf '\r' >&"$_rewindln_fd"
	fi
)


#
# Show and update a spinner on the given file descriptor (default: 2),
# but only if that file is a teletype device and $quiet is unset.
#
# Usage:
#	spin [fd]
#
# Globals:
#	$quiet  Be quiet?
#
spin() {
	_spin_fd="${1:-2}"

	if ! [ "$quiet" ] && [ -t "$_spin_fd" ]
	then
		: "${_spin_counter:=0}"

		case $((_spin_counter % 4)) in
		(0) printf '\r-\r'  >&"$_spin_fd" ;;
		(1) printf '\r\\\r' >&"$_spin_fd" ;;
		(2) printf '\r|\r'  >&"$_spin_fd" ;;
		(3) printf '\r/\r'  >&"$_spin_fd" ;;
		esac

		_spin_counter=$((_spin_counter + 1))
	fi
	unset _spin_fd
}


#
# Remove a lock file.
#
# Usage:
#	unlock file
#
unlock() (
	_unlock_file="${1:?}"
	if read -r _unlock_pid <"$_unlock_file" &&
	   [ "$_unlock_pid" -eq $$ ]
	then rm -f "$_unlock_file"
	fi
)


#
# Print a message to standard error.
#
# Options:
#	-c sgi  Colorize message with SGI code.
#	-n      Suppress the terminating newline.
#	-q      Suppress output if $quiet is set.
#	-r	Read arguments for format from standard input.
#	-v      Suppress output unless $verbose is set.
#
warn() (
	: "${1:?}"

	OPTIND=1 OPTARG='' _warn_opt=''
	while getopts 'c:nqrv' _warn_opt
	do
		case $_warn_opt in
		(n)	_warn_nl= ;;
		(c)	[ "${colors-0}" -ge 8 ] && [ -t 2 ] &&
			_warn_col="\\033[${OPTARG}m" _warn_res='\033[0m' ;;
		(q)	[ "${quiet-}" ]   && return 0 ;;
		(r)	_warn_read=y ;;
		(v)	[ "${verbose-}" ] || return 0 ;;
		(*)	return 1
		esac
	done
	shift $((OPTIND - 1))

	: "${progname:="${0##*/}"}"
	: "${_warn_col=}"
	: "${_warn_nl=\\n}"
	: "${_warn_read=}"
	: "${_warn_res=}"

	_warn_fmt="$progname: $_warn_col$1$_warn_res$_warn_nl"
	shift 1

	# shellcheck disable=2059
	if [ "$_warn_read" ]
	then
		while IFS= read -r _warn_line
		do printf -- "$_warn_fmt" "$_warn_line" >&2
		done
	else
		printf -- "$_warn_fmt" "$@" >&2
	fi
)
