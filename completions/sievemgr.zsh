#compdef sievemgr
# sievemgr.zsh - completion script for ZSh

#
# Copyright 2024  Odin Kroeger
#
# This file is part of SieveManager.
#
# SieveManager is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# SieveManager is distributed in the hope that it will be useful,
# but WITHOUT ALL WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SieveManager. If not, see <https://www.gnu.org/licenses/>.
#

_sievemgr() {
	local context state state_descr line
	typeset -A opt_args

	_arguments -C -S -s : \
		'-C[Do not overwrite files.]'				\
		'-N[Set filename of .netrc file]: :_files'		\
		'-V[Show version.]'					\
		'-c[Read configuration from file]: :_files'		\
		'-d[Enable debugging mode.]'				\
		'(-s 2 *)-e[Execute expression.]: :( )'			\
		'-f[Overwrite and remove files without confirmation.]'	\
		'-h[Show help.]'					\
		'-i[Confirm removing or overwriting files.]'		\
		'-o[Set configuration option.]: :(
			alias=
			backups=
			cafile=
			cadir=
			cert=
			confdir=
			confirm=
			getkeypass=
			getpass=
			host=
			key=
			login=
			ocsp=no
			ocsp=yes
			port=
			tls=no
			tls=yes
			saslmechs=
			saslprep=usernames
			saslprep=passwords
			saslprep=all
			saslprep=none
			verbosity=auth
			verbosity=debug
			verbosity=error
			verbosity=info
			verbosity=warning
			x509strict=no
			x509strict=yes
		)'							\
		'-q[Be quieter.]'					\
		'(-e 2 *)-s[Read commands from file]: :_files'		\
		'-v[Be more verbose.]'					\
		'1::account:->account'	\
		'2::command:(
			about
			activate
			caps
			cat
			cd
			cert
			check
			cmp
			cp
			deactivate
			diff
			ed
			exit
			get
			help
			ls
			more
			mv
			put
			python
			rm
			sh
			su
			vi
			xargs
		)'

	case $state in
	(account)
		local home="${HOME-}"
		[ "$home" ] && eval home=~
		local xdg_data_home="${XDG_DATA_HOME:-"$home/.config"}"
		local accounts=()
		local config key value

		for config in				\
			"/etc/sieve/config"		\
			"/etc/sieve.cf"			\
			"$xdg_data_home/sieve/config"	\
			"$home/.sieve/config"		\
			"$home/.sieve.cf"
		do
			[ -e "$config" ] || continue
			while read -r key value
			do
				case $key in (account|alias)
					[ "$value" ] && accounts+=("$value")
				esac
			done <"$config"
		done

		_describe '' accounts
		;;
	esac
} && compdef _sievemgr sievemgr
