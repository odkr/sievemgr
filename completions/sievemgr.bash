# sievemgr.bash - Bash completion script for SieveManager

#
# Copyright 2024  Odin Kroeger
#
# This file is part of SieveManager.
#
# SieveManager is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# SieveManager is distributed in the hope that it will be useful,
# but WITHOUT ALL WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SieveManager. If not, see <https://www.gnu.org/licenses/>.
#

_comp_sievemgr() {
	local cmd="$1" word="$2" pre="$3"

	set -- $COMP_LINE
	shift

	local argdel=0
	while [ $# -gt 0 ]
	do
		case $1 in
		(--)		shift
				break
				;;
		(-*[co])	local optopt="${1: -1}"
				local optarg="${1#-*$optopt}"
				if [ "$optarg" ]
				then
					shift
				elif
					[ $# -gt 1 ]
				then
					shift 2
				else
					shift
					argdel=1
				fi
				;;
		(-*)		local optopt="${1#-}"
				shift ;;
		(*)		break
		esac
	done

	case $COMP_LINE in
	(*" ")	local argidx=$(($# - argdel + 1)) ;;
	(*)	local argidx=$(($# - argdel)) ;;
	esac

	local replies
	case $argidx in
	(0)	case $word in
		(--)		COMPREPLY=()
				return
				;;
		(-)		COMPREPLY=(-N -C -V -c -d -e -f
		                           -h -i -o -q -s -v)
				return
				;;
		(-*[co])	:
				;;
		(-*)		COMPREPLY=("$word")
				return
				;;
		esac

		case $optopt in
		(o)	replies=(
				'alias='
				'backups='
				'cafile='
				'cadir='
				'cert='
				'confdir='
				'confirm='
				'getkeypass='
				'getpass='
				'host='
				'key='
				'login='
				'ocsp=no'
				'ocsp=yes'
				'port='
				'tls=no'
				'tls=yes'
				'saslmechs='
				'saslprep=usernames'
				'saslprep=passwords'
				'saslprep=all'
				'saslprep=none'
				'verbosity=auth'
				'verbosity=debug'
				'verbosity=error'
				'verbosity=info'
				'verbosity=warning'
				'x509strict=no'
				'x509strict=yes'
			)
			;;
		(*)	COMPREPLY=("$word")
			return
		esac
		;;
	(1)	local home="${HOME-}"
		[ "$home" ] && eval home=~
		local xdg_data_home="${XDG_DATA_HOME:-"$home/.config"}"
		local config key value

		for config in				\
			"/etc/sieve/config"		\
			"/etc/sieve.cf"			\
			"$xdg_data_home/sieve/config"	\
			"$home/.sieve/config"		\
			"$home/.sieve.cf"
		do
			[ -e "$config" ] || continue
			while read -r key value
			do
				case $key in
				(account|alias)	: ;;
				(*)		continue
				esac

				case $word in
				(*@*)	local netloc="${word##*@}"
					case $value in ("$netloc"*)
						COMPREPLY+=("${word%@*}@$value")
					esac
					;;
				(*)	case $value in ("$word"*)
						COMPREPLY+=("$value")
					esac
					;;
				esac
			done <"$config"
		done
		return
		;;
	(2)	local replies=(
			'about'
			'activate'
			'caps'
			'cat'
			'cd'
			'cert'
			'check'
			'cmp'
			'cp'
			'deactivate'
			'diff'
			'ed'
			'exit'
			'get'
			'help'
			'ls'
			'more'
			'mv'
			'put'
			'python'
			'rm'
			'sh'
			'su'
			'vi'
			'xargs'
		)
		;;
	esac

	local reply
	for reply in "${replies[@]}"
	do
		case $reply in
		("$word"*) COMPREPLY+=("$reply")
		esac
	done

	if	[ "$optopt" = o ]		&&
		[ "${#COMPREPLY[@]}" -eq 1 ]	&&
		[ "${COMPREPLY[0]: -1}" = '=' ]
	then
		compopt -o nospace
	fi
} && complete -F _comp_sievemgr -o default sievemgr
