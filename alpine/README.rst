********************
Alpine Linux package
********************

.. highlight:: none

The :file:`alpine` sub-directory contains instructions
for building Alpine Linux packages.


Requirements
============

The :file:`Makefile` requires `GNU Make`_, which is the default Make
under Debian and will be installed automatically if you follow the
instructions below.


Preparation
===========

Install build and packaging tools::

    doas apk add alpine-sdk

Generate or import security keys::

    abuild-keygen -a -i


Build
=====

Build the packages::

    make
