Installation
------------

Home directory
^^^^^^^^^^^^^^

Install SieveManager to your home directory::

    make userinstall

The manual pages for SieveManager are copied to :file:`~/.local/share/man`, the
Bash completion script to :file:`~/.local/share/bash-completion/completions`,
and the Zsh completion script to :file:`~/.local/share/zsh/site-functions`.


Virtual environment
^^^^^^^^^^^^^^^^^^^

Create a virtual environment in :file:`/opt/odkr/sievemgr` and
install SieveManager to that environment::

    sudo make venvdir=/opt/odkr/sievemgr venvinstall

.. tip::
    Install SieveManager to :file:`/foo` and its manual and completion
    scripts to their standard locations in :file:`/usr/local`::

        sudo make venvdir=/foo prefix=/usr/local venvinstall


Manual
^^^^^^

SieveManager can also be installed by being copied into a
directory in your :envvar:`PATH`::

    install -m u=rwx,go=rx sievemgr.py ~/.local/bin/sievemgr

However, the cryptography_ module is required to check whether a server's TLS
certificate has been revoked and the dnspython_ module is required to resolve
DNS `SRV records`_. So you should install those modules, too::

    pip3 install --user cryptography dnspython


De-installation
---------------

Uninstall SieveManger::

    make uninstall

.. note::
    The :mod:`cryptography` and :mod:`dnspython` modules are only
    removed if they were installed by :command:`make venvinstall`.


Troubleshooting
---------------

The most common reason for :command:`make userinstall` and
:command:`make venvinstall` to fail is that PyCA_ does not
offer a pre-compiled wheel of the cryptography_ module for
your system.

Install SieveManager `manually <#manual>`_ instead (see above)
and do *not* install the cryptography_ module.

.. danger::
    SieveManager works witout :mod:`cryptography`. But it will *not*
    be able to check whether TLS certificates has been revoked.


.. _cryptography: https://cryptography.io

.. _dnspython: https://www.dnspython.org
