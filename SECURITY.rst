********
Security
********

Connections are secured with Transport Layer Security by default.
Insecure configuration settings and password-handling practices
are advised against in the documentation.

Caveats
=======

Credentials are stored in memory so that they need not be entered again
in case of a referral. However, because page-locking is unfeasible in
Python, they may be swapped out to the disk.

SieveManager can query password managers to automate logins. However,
any command that can be run by :command:`sievemgr` can, at the very least,
also be run by any application that can run :command:`python`.


Versions
========

Only the most recent version of SieveManager receives security updates.

.. TIP::
    Subscribe to https://codeberg.org/odkr/sievemgr/releases.rss
    to be notified about new releases.


Reporting
=========

If you have discovered a security issue, please write an encrypted email
to the :file:`@zoho.com` address listed in my `PGP key`_.


.. _`PGP key`: https://keys.openpgp.org/vks/v1/by-fingerprint/8975B184615BC48CFA4549056B06A2E03BE31BE9

