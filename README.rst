.. image:: https://www.bestpractices.dev/projects/8336/badge
   :target: https://www.bestpractices.dev/en/projects/8336
   :alt: OpenSSF best practices badge

############
SieveManager
############

Sieve is a programming language for filtering email. Sieve scripts are
typically run by the mail server when mail is delivered to an inbox,
so they need to be managed remotely.

SieveManager is a command-line client and Python module for uploading,
downloading, and managing remote Sieve scripts using the ManageSieve
protocol.

It aims to be convenient, secure, and portable,
and respects Unix as well as Python conventions.

**WARNING**: The command-line interface, the configuration semantics,
and the Python API may still change.

.. _Sieve: http://sieve.info
.. _`home page`: https://odkr.codeberg.page/sievemgr


Example
=======

Upload and activate a Sieve script:

.. code:: none

    $ sievemgr user@imap.foo.example
    user@imap.foo.example's password: <password>
    sieve://user@imap.foo.example> put script.sieve
    sieve://user@imap.foo.example> activate script.sieve

In Python:

.. code:: python

    from sievemgr import SieveManager
    with SieveManager('imap.foo.example') as mgr:
        mgr.authenticate('user', 'password')
        with open('sieve.script', mode='br') as script:
            mgr.putscript(script, 'sieve.script')
        mgr.setactive('sieve.script')


Features
========

* Fully implements RFC 5804 (ManageSieve protocol)

* Login can be automated with:

  * Password managers
  * GnuPG-encrypted password files
  * Configuration files
  * ``.netrc``

* Password-based authentication with:

  * CRAM-MD5
  * LOGIN
  * PLAIN
  * SCRAM-\* and SCRAM-\*-PLUS [#untested]_ with

    * SHA-1
    * SHA-2-234
    * SHA-2-256
    * SHA-2-384
    * SHA-2-512
    * SHA-3-512

* TLS client authentication

* Proxy authentication

* Powerfull tab-completion

* Highly scriptable

* Emacs-like backup of scripts

* Checks whether TLS certificates have been revoked
  (using lightweight OCSP)

* Supports giving IPv6 addresses on the command-line


.. [#untested] SCRAM-\*-PLUS authentication is untested.


Documentation
=============

Use **sievemgr -h**, type "help" in the SieveManager shell,
or see the `home page`_.


Contact
=======

Home page:
    https://odkr.codeberg.page/sievemgr

Issue tracker:
    https://github.com/odkr/sievemgr/issues

Source code (primary):
    https://codeberg.org/odkr/sievemgr

Source code (secondary):
    https://repo.or.cz/sievemgr.git


License
=======

Copyright 2023 and 2024  Odin Kroeger

SieveManager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the FreeSoftware Foundation, either version 3 of the License, or (at
your option) any later version.

SieveManager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SieveManager. If not, see <https://www.gnu.org/licenses/>.

