.POSIX:


#
# Special targets
#

.PHONY: \
	check \
	clean \
	coverage \
	dependcheck \
	dist \
	distcheck \
	distclean \
	deb \
	debupload \
	docs \
        install \
	install-completions \
	install-man \
	install-strip \
	install-module \
	installdirs \
	lint \
	maintainer-clean \
	mostlyclean \
	venvinstall \
	wheelupload \
	pre-dist \
	pre-install \
	post-uninstall \
	quickcheck \
	quicklint \
	uninstall \
	uninstall-module \
	uninstalldirs \
	uninstallfiles \
	uninstallvenv \
	userinstall

.SUFFIXES:

all:
	:


#
# General macros
#

PYTHON = python3
SHELL = /bin/sh
VPATH = $(srcdir)

package = sievemgr
version = 0.8

pydir = venv
pip = $(pydir)/bin/pip
activate = ./$(pydir)/bin/activate
srcdir = .
srcs = sievemgr.py

$(activate) $(devpip):
	$(PYTHON) -mvenv venv
	. $(activate) && pip install -U pip


#
# Installation
#

INSTALL = install
PIP = pip
PIPFLAGS = --root=$(DESTDIR) --prefix=$(prefix)
PREFIX = /usr/local

bashcompdir = $(datarootdir)/bash-completion/completions
bindir = $(exec_prefix)/bin
datarootdir = $(prefix)/share
exec_prefix = $(prefix)
installdirs = \
	$(DESTDIR)$(man1dir) \
	$(DESTDIR)$(man5dir) \
	$(DESTDIR)$(bashcompdir) \
	$(DESTDIR)$(zshcompdir)
instlog = install.log
man1dir = $(mandir)/man1
man1ext = .1
man5dir = $(mandir)/man5
man5ext = .5
mandir = $(datarootdir)/man
prefix = $(PREFIX)
venvdir = /opt/odkr/$(package)
zshcompdir = $(datarootdir)/zsh/site-functions

install: \
	pre-install \
	install-module \
	install-man \
	install-completions

installcheck:
	$(MAKE) -e instlog=instchk.log venvdir=stage venvinstall
	stage/bin/sievemgr --version | grep -Fq 'SieveManager $(version)'
	man ./stage/share/man/man1/sievemgr.1 | col -b | grep -Fq sievemgr
	man ./stage/share/man/man5/sieve.cf.5 | col -b | grep -Fq sieve.cf
	$(MAKE) -e instlog=instchk.log uninstall

install-man: \
	installdirs \
	$(DESTDIR)$(man1dir)/sievemgr$(man1ext) \
	$(DESTDIR)$(man5dir)/sieve.cf$(man5ext)

uninstall: \
	uninstall-module \
	uninstallfiles \
	uninstalldirs \
	uninstallvenv \
	post-uninstall

install-completions: \
	installdirs \
	$(DESTDIR)$(bashcompdir)/sievemgr.bash \
	$(DESTDIR)$(zshcompdir)/_sievemgr

install-module:
	$(NORMAL_INSTALL)
	$(PIP) install $(PIPFLAGS) $(srcdir)
	printf 'pip: %s\n' $(PIP) >>$(instlog)

install-strip: install

pre-install:
	$(PRE_INSTALL)
	set -C && : >$(instlog)

installdirs: $(installdirs)

userinstall:
	$(MAKE) -e PIPFLAGS=--user prefix=~/.local install

venvinstall:
	$(MAKE) -e PIP=$(venvdir)/bin/pip PIPFLAGS= PREFIX=$(venvdir) \
		pre-install $(venvdir)/bin/pip installdirs \
		install-module install-man install-completions

uninstall-module:
	$(NORMAL_UNINSTALL)
	-"$$(sed -n 's/pip: //p' $(instlog))" uninstall -y $(package)

uninstalldirs:
	$(POST_UNINSTALL)
	- sed -n 's/^dir: //p' $(instlog) | xargs -E '' rmdir -pv

uninstallfiles:
	$(POST_UNINSTALL)
	sed -n 's/^file: //p' $(instlog) | xargs -E '' rm -f

uninstallvenv:
	$(POST_UNINSTALL)
	if venv="$$(sed -n 's/^venv: //p' $(instlog))" && [ "$$venv" ]; \
	then rm -rf "$$venv"; \
	fi

post-uninstall:
	$(POST_UNINSTALL)
	rm $(instlog)

$(DESTDIR)$(man1dir)/sievemgr$(man1ext): man/sievemgr.1
	$(POST_INSTALL)
	$(INSTALL) -m u=rw,go=r man/sievemgr.1 $@
	printf 'file: %s\n' $@ >>$(instlog)

$(DESTDIR)$(man5dir)/sieve.cf$(man5ext): man/sieve.cf.5
	$(POST_INSTALL)
	$(INSTALL) -m u=rw,go=r man/sieve.cf.5 $@
	printf 'file: %s\n' $@ >>$(instlog)

$(DESTDIR)$(bashcompdir)/sievemgr.bash: completions/sievemgr.bash
	$(POST_INSTALL)
	$(INSTALL) -m u=rw,go=r completions/sievemgr.bash $@
	printf 'file: %s\n' $@ >>$(instlog)

$(DESTDIR)$(zshcompdir)/_sievemgr: completions/sievemgr.zsh
	$(POST_INSTALL)
	$(INSTALL) -m u=rw,go=r completions/sievemgr.zsh $@
	printf 'file: %s\n' $@ >>$(instlog)

$(installdirs):
	$(PRE_INSTALL)
	mkdir -pm0755 $@
	printf 'dir: %s\n' $@ >>$(instlog)

$(venvdir)/bin/pip: $(venvdir)/bin/activate
	$(PRE_INSTALL)
	. $(venvdir)/bin/activate && pip install -U pip

$(venvdir)/bin/activate:
	$(PRE_INSTALL)
	$(PYTHON) -mvenv $(venvdir)
	printf 'venv: %s\n' $(venvdir) >>$(instlog)


#
# Documentation
#

CURL = curl
COVERAGE = $(pydir)/bin/coverage
SPHINX = $(pydir)/bin/sphinx-build

badgeurl = https://img.shields.io/badge/coverage-{}%25-lightgrey
covreportdir = docs/_static/coverage
covreports = docs/_static/coverage.svg $(covreportdir)/index.html

docs: $(activate) $(SPHINX)
	. $(activate); \
	( cd docs && $(MAKE) -e html ); \
	$(SPHINX) $(SPHINXFLAGS) -Dmanpages_url= -bman docs man

$(SPHINX): $(activate) $(pip)
	. $(activate) && pip install -r docs/sphinx.txt

coverage: $(covreports)

$(covreportdir)/index.html: .coverage $(activate) $(COVERAGE)
	. $(activate) && \
		$(COVERAGE) html --include sievemgr.py -d $(covreportdir)

docs/_static/coverage.svg: .coverage $(activate) $(COVERAGE)
	. $(activate) && \
		$(COVERAGE) report --include sievemgr.py | \
		awk '$$1 == "TOTAL" {gsub(/%/, "", $$4); print $$4}' | \
		xargs -I '{}' $(CURL) -o $@ $(badgeurl)

.coverage: sievemgr.py $(tests) $(autotests) $(activate) $(COVERAGE)
	. $(activate); \
	pip install -U cryptography dnspython; \
	$(COVERAGE) run -m unittest discover

$(COVERAGE): $(activate) $(pip)
	. $(activate) && pip install coverage


#
# Distribution
#

GPG = gpg -ab
GPGFLAGS = -q --batch --yes
PATHCHK = pathchk
PATHCHKFLAGS = -Pp
SAFETY = safety
SAFETYFLAGS = --disable-optional-telemetry --policy-file=$(safetypolicy)
TAR = pax -ws '/^/$(distname)\//' -x ustar
UNTAR = tar -x
UNZIP = gunzip -c
ZIP = gzip

distar = $(distname).tgz
distfiles = \
	$(srcs) \
	$(tests) \
	$(autotests) \
	INSTALL.rst \
	LICENSE.rst \
	Makefile \
	NEWS.rst \
	README.rst \
	alpine/APKBUILD \
	alpine/Makefile \
	alpine/README.rst \
	completions/sievemgr.bash \
	completions/sievemgr.zsh \
	debian/Makefile \
	debian/README.rst \
	debian/control.in \
	debian/copyright \
	debian/lintian.cfg \
	docs/Makefile \
	docs/_static/custom.css \
	docs/changelog.rst \
	docs/command.rst \
	docs/conf.py \
	docs/config.rst \
	docs/contrib.rst \
	docs/feedback.rst \
	docs/index.rst \
	docs/install.rst \
	docs/license.rst \
	docs/module.rst \
	docs/security.rst \
	docs/sphinx.txt \
	linters.txt \
	man/sieve.cf.5 \
	man/sievemgr.1 \
	pyproject.toml \
	scripts/mkwords \
	setup.cfg
distname = $(package)-$(version)
safetypolicy = safety.yml

dist: pre-dist $(distar) $(distar).asc

distcheck: dist

pre-dist: lint check dependcheck installcheck docs
	! grep -i FIXME $(lint)

$(distar): lint check $(distfiles)

$(distar).asc: $(distar)

dependcheck:
	$(SAFETY) scan $(SAFETYFLAGS) || [ $$? -eq 127 ]

distcheck:
	$(UNZIP) $(distar) | $(UNTAR)
	cd $(distname) && $(MAKE) -e check
	rm -rf $(distname)

$(distar):
	[ "$$($(PYTHON) sievemgr.py --version | \
		awk 'NR == 1 {print $$2; exit}')" = "$(version)" ]
	$(PATHCHK) $(PATHCHKFLAGS) $(distfiles) || [ $$? -eq 127 ]
	$(TAR) $(TARFLAGS) $(distfiles) | $(ZIP) $(ZIPFLAGS) >$(distar)

$(distar).asc:
	$(GPG) $(GPGFLAGS) $(distar)


#
# PyPI
#

PYPI_UPLOAD = $(TWINE) upload $(TWINEFLAGS)
TWINE = twine
TWINEFLAGS = -s -rtestpypi

wheel: lint check
	$(PYTHON) -m build

upload: wheel
	$(PYPI_UPLOAD) dist/$(distname).tar.gz dist/$(distname)-py3-none-any.whl


#
# Cleanup
#

maintainer-clean: distclean
	@echo '$@ is for maintainers only!'
	@echo 'It deletes files that cannot be built with standard tools.'
	rm -rf $(pydir) docs/_build/html/*
	rm -f man/sievemgr.1 man/sieve.cf.5 docs/build/html/.buildinfo

distclean: clean
	rm -f tags

clean: mostlyclean
	find . -type d -name __pycache__ -exec rm -rf '{}' +
	rm -rf $(package).egg-info .*_cache
	rm -f .coverage

mostlyclean: tidy
	rm -rf build dist
	rm -f $(package)-*.tgz $(package)-*.tgz.asc

tidy:
	find . -type f -name '*.bak' -exec rm '{}' +


#
# Tags
#

CTAGS = ctags

TAGS: tags

tags: $(srcs)
	$(CTAGS) -G $(CTAGSFLAGS) $(srcs)


#
# Tests
#

checks = discover
tests = \
	tests/__init__.py \
	tests/test_acap.py \
	tests/test_backup.py \
	tests/test_log.py \
	tests/test_sasl.py \
	tests/test_shell.py \
	tests/test_sieve.py \
	tests/test_term.py
autotests = \
	tests/auto/__init__.py \
	tests/auto/words.py

check: $(autotests)
	$(PYTHON) -m unittest $(checks)

quickcheck:
	QUICKTEST=y $(PYTHON) -m unittest $(checks)

tests/auto/words.py:
	scripts/mkwords -o$@


#
# Linters
#

BANDIT = bandit
BANDITFLAGS = -qc pyproject.toml
FLAKE8 = flake8
MYPY = mypy
PYLINT = pylint
PYRIGHT = pyright
RUFF = ruff
RUFFFLAGS = --quiet
VERMIN = vermin
VULTURE = vulture

lint = $(srcs) $(tests)

lint: quicklint

lint:
	$(PYRIGHT) $(PYRIGHTFLAGS) $(lint) || [ $$? -eq 127 ]
	$(FLAKE8) $(FLAKE8FLAGS) $(lint) || [ $$? -eq 127 ]
	$(VERMIN) $(VERMINFLAGS) $(lint) || [ $$? -eq 127 ]
	$(VULTURE) $(VULTUREFLAGS) $(lint) || [ $$? -eq 127 ]
	$(PYLINT) $(PYLINTFLAGS) $(lint) || [ $$? -eq 127 ]

quicklint:
	$(MYPY) $(MYPYFLAGS) $(lint) || [ $$? -eq 127 ]
	$(BANDIT) $(BANDITFLAGS) $(lint) || [ $$? -eq 127 ]
	$(RUFF) check $(RUFFFLAGS) $(lint) || [ $$? -eq 127 ]
